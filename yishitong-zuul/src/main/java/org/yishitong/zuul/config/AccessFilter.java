package org.yishitong.zuul.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.http.HttpServletRequestWrapper;
import com.netflix.zuul.http.ServletInputStreamWrapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.yishitong.common.response.CommonEnum;
import org.yishitong.common.response.Response;
import org.yishitong.common.utils.CommonConstant;
import org.yishitong.common.utils.DesUtils;
import org.yishitong.common.utils.RSAUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.util.StreamUtils;

import javax.annotation.Resource;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.UUID;

/**
 * @author arch
 */
public class AccessFilter extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(AccessFilter.class);

    @Resource(name = "passHeader")
    private List<String> passHeader;

    @Resource(name = "passToken")
    private List<String> passToken;

    @Resource(name = "hitPath")
    private List<String> hitPath;

    @Value("${user.login.tokenTime}")
    private long validTime;

    @Value("${ras.preKey}")
    private String privateKey;

    @Value("${desKey}")
    private String desKey;

    private static RSAPrivateKey rSAPrivateKey;

    private static String NULL = "null";

    @Override
    public Object run() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest httpServletRequest = requestContext.getRequest();
        String headerToken = httpServletRequest.getHeader(CommonConstant.HEADER_TOKEN_ID);
        if (StringUtils.isBlank(headerToken)) {
            Response response = Response.error(CommonEnum.TOKEN_ERROR);
            String msg = JSON.toJSONString(response);
            encodeError(requestContext, msg);
            return null;
        }
        boolean isHitToken = headerToken.contains(CommonConstant.HIT_TOKEN_PREFIX);
        boolean isHitPath = hitPath.contains(httpServletRequest.getServletPath().split("\\?")[0]);
        boolean errorLength = headerToken.length() != CommonConstant.TOKEN_LENGTH
                && headerToken.length() != CommonConstant.TOKEN_LENGTH_HIT;
        if (errorLength || isHitToken && !isHitPath || !isHitToken && isHitPath) {
            Response response = Response.error(CommonEnum.TOKEN_ERROR);
            String msg = JSON.toJSONString(response);
            encodeError(requestContext, msg);
        } else {
            requestContext.addZuulRequestHeader(CommonConstant.HEADER_AUTH_TOKEN, CommonConstant.STATUS_YES);
            decrypt(requestContext, httpServletRequest);
        }
        return null;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        StringBuffer url = request.getRequestURL();
        String tempContextUrl = request.getScheme() + "://" + request.getServerName();
        String trackId = UUID.randomUUID().toString();
        ctx.set(CommonConstant.ZUUL_TRACK_ID, trackId);
        ctx.addZuulRequestHeader(CommonConstant.HEADER_REALLY_URL, tempContextUrl);
        logger.info("Zuul:AccessFilter:::请求真实地址requestUrl={}，trackId={}", url, trackId);
        String requestUrl = request.getServletPath().split("\\?")[0];
        init3DesKey(ctx, request);

        try {
            InputStream in = request.getInputStream();
            String reqBody = StreamUtils.copyToString(in, Charset.forName("UTF-8"));
            logger.info("reqBody=" + reqBody);
        } catch (IOException e) {
            logger.error("获取请求体异常！", e);
        } catch (Exception e) {
            logger.error("获取请求体异常！", e);
        }
        logger.info("hitPath=" + JSON.toJSONString(hitPath));

        // 统一对请求头进行参数校验，所有请求必须携带operatorId和appId
        boolean headerError = true;
        // 如果是碰一碰请求地址，则只判断operatorId
//        if (hitPath.contains(requestUrl)) {
//            headerError = StringUtils.isBlank(request.getHeader(CommonConstant.HEADER_OPERATOR_ID));
//        }
        //过滤通用api接收数据接口
        if (requestUrl.indexOf("/rxd/common")>-1) {
            logger.info("Zuul:AccessFilter:::没有拦截url，requestUrl={}，trackId={}", requestUrl, trackId);
            ctx.addZuulRequestHeader(CommonConstant.HEADER_AUTH_TOKEN, CommonConstant.STATUS_NO);
            decrypt(ctx, request);
            return false;
        }
        if (!passHeader.contains(requestUrl) && headerError) {
            Response response = Response.error(CommonEnum.INVALID_HEAD_PARAM);
            String msg = JSON.toJSONString(response);
            encodeError(ctx, msg);
        }
        if (passToken.contains(requestUrl)) {
            logger.info("Zuul:AccessFilter:::没有拦截url，requestUrl={}，trackId={}", requestUrl, trackId);
            ctx.addZuulRequestHeader(CommonConstant.HEADER_AUTH_TOKEN, CommonConstant.STATUS_NO);
            decrypt(ctx, request);
            return false;
        }

        return true;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    private void init3DesKey(RequestContext requestContext, HttpServletRequest httpServletRequest) {
        String trackId = (String) requestContext.get(CommonConstant.ZUUL_TRACK_ID);
        if (StringUtils.isNotBlank(httpServletRequest.getHeader(CommonConstant.ENCRY_PT_RSA))) {
            logger.info("Zuul:AccessFilter:::当前请求为加密请求，开始对加密请求的3desKey进行处理。trackId={}", trackId);
            try {
                if (rSAPrivateKey == null) {
                    rSAPrivateKey = RSAUtils.getPrivateKey(privateKey);
                }
                String key = RSAUtils.privateDecrypt(httpServletRequest.getHeader(CommonConstant.ENCRY_PT_RSA),
                        rSAPrivateKey);
                requestContext.addZuulRequestHeader(CommonConstant.DES_KEY, key);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                logger.error("Zuul:AccessFilter:::zuul解密3desKey数据异常,trackId=" + trackId, e);
                encodeError(requestContext, "系统异常：" + e.getMessage(), 500);
            }
        }
    }

    private void decrypt(RequestContext requestContext, HttpServletRequest httpServletRequest) {
        String trackId = (String) requestContext.get(CommonConstant.ZUUL_TRACK_ID);
        try {
            InputStream in = httpServletRequest.getInputStream();

            String charset = getCharset(requestContext);

            String reqBody = StreamUtils.copyToString(in, Charset.forName(charset));
            logger.info("Zuul:AccessFilter:::请求体：{},trackId={}", reqBody, trackId);
            if (requestContext.getZuulRequestHeaders().containsKey(CommonConstant.DES_KEY)) {
                String key = requestContext.getZuulRequestHeaders().get(CommonConstant.DES_KEY);
                reqBody = DesUtils.decode3Des(key, reqBody);
                logger.info("Zuul:AccessFilter:::请求体解密后：{},trackId={}", reqBody, trackId);
            }
            String dJson =  reqBody ;
            final byte[] reqBodyBytes = dJson.getBytes(charset);
            requestContext.setRequest(new HttpServletRequestWrapper(requestContext.getRequest()) {
                @Override
                public ServletInputStream getInputStream() {
                    return new ServletInputStreamWrapper(reqBodyBytes);
                }

                @Override
                public int getContentLength() {
                    return reqBodyBytes.length;
                }

                @Override
                public long getContentLengthLong() {
                    return reqBodyBytes.length;
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void encodeError(RequestContext requestContext, String body) {
        encodeError(requestContext, body, 200);
    }

    /**
     * 对返回异常信息处理
     *
     * @param requestContext
     * @param body
     */
    private void encodeError(RequestContext requestContext, String body, int httpStatus) {
        String trackId = (String) requestContext.get(CommonConstant.ZUUL_TRACK_ID);
        requestContext.getResponse().setCharacterEncoding("utf-8");
        requestContext.getResponse().setContentType("application/json;charset=UTF-8");
        requestContext.setSendZuulResponse(false);
        requestContext.setResponseStatusCode(httpStatus);
        if (requestContext.getZuulRequestHeaders().containsKey(CommonConstant.DES_KEY)) {
            logger.info("Zuul:AccessFilter:::对返回异常信息【" + body + "】进行加密处理！trackId={}", trackId);
            try {
                body = DesUtils.encode3Des(requestContext.getZuulRequestHeaders().get(CommonConstant.DES_KEY), body);
                requestContext.setResponseDataStream(IOUtils.toInputStream("{\"data\":\"" + body + "\"}"));
            } catch (UnsupportedEncodingException e) {
                logger.error("Zuul:AccessFilter:::对返回异常信息进行加密出错！", e);
                requestContext.setResponseDataStream(IOUtils.toInputStream("对返回异常信息进行加密出错！"));
            }
        } else {
            requestContext.setResponseDataStream(IOUtils.toInputStream(body));
        }
    }



    /**
     * 对特定URL进行特殊编码处理
     *
     * @param requestContext
     * @return
     */
    String getCharset(RequestContext requestContext) {
        String reqUrl = requestContext.getRequest().getServletPath().split("\\?")[0];
        String charset = "UTF-8";
        /*if (reqUrl.contains("/rxd/shandejiufu/trans")) {
            charset = "GBK";
        }*/
        return charset;
    }
}
