package org.yishitong.zuul.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.yishitong.common.utils.CommonConstant;
import org.yishitong.common.utils.DesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.POST_TYPE;

/**
 * @author arch
 */
public class BackFilter extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(BackFilter.class);

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String trackId = (String) ctx.get(CommonConstant.ZUUL_TRACK_ID);
        StringBuffer url = ctx.getRequest().getRequestURL();
        Map<String, String> mh = ctx.getZuulRequestHeaders();
        try {
            InputStream stream = ctx.getResponseDataStream();
            String body = StreamUtils.copyToString(stream, Charset.forName("UTF-8"));
            logger.info("Zuul:BackFilter:::请求地址：" + url + "从流中获取返回体数据:" + body + "，trackId：" + trackId);
            if (mh.containsKey(CommonConstant.DES_KEY)) {
                body = DesUtils.encode3Des(mh.get(CommonConstant.DES_KEY), body);
                logger.info("Zuul:BackFilter:::请求地址：{}，从流中获取返回体数据解密后：{}，trackId：{}。", url, body, trackId);
                ctx.setResponseBody("{\"data\":\"" + body + "\"}");
            } else {
                ctx.setResponseBody(body);
            }
            ctx.setResponseStatusCode(200);
        } catch (IOException e) {
            logger.error("zuul返回数据加密异常！", e);
        }
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public String filterType() {
        return POST_TYPE;
    }

}
