package org.yishitong.zuul.config;

import com.alibaba.fastjson.JSON;
import org.yishitong.common.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Component
public class UserFallback implements FallbackProvider {

    private static final Logger logger = LoggerFactory.getLogger(UserFallback.class);

    @Override
    public String getRoute() {
        return "yishitong-user";
    }

    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        logger.error("失败route:{},原因:{}", route, cause.getMessage());
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() {
                return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() {
                return 897;
            }

            @Override
            public String getStatusText() {
                return "OK";
            }

            @Override
            public void close() {

            }

            @Override
            public InputStream getBody() {
                logger.error("调用user出现异常！", cause);
                Response response = Response.errorMeg("网络连接异常，刷新重试[U]！");
                return new ByteArrayInputStream(JSON.toJSONString(response).getBytes());
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                return headers;
            }
        };
    }
}
