package org.yishitong.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.ImportResource;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.yishitong.zuul.config.AccessFilter;
import org.yishitong.zuul.config.BackFilter;

@ImportResource("applicationContext.xml")
@SpringBootApplication( scanBasePackages = "org.yishitong")
@EnableDiscoveryClient
@EnableZuulProxy
//@PropertySource(value={"file:/home/config/dbconfig.properties"},ignoreResourceNotFound = true)
@PropertySource(value={"classpath:application.properties"},ignoreResourceNotFound = true)
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();
    }

    @Bean
    public BackFilter backFilter() {
        return new BackFilter();
    }
}
