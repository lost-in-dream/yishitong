package org.yishitong.common.datapage;

/**
 * SQLServer2005的分页数据库方言实现
 * @author PanLitao
 */
public class SQLServerDialect implements Dialect {

    @Override
    public boolean supportsLimit() {
        return true;
    }

    static int getAfterSelectInsertPoint(String sql) {
        int selectIndex = sql.toLowerCase().indexOf("select");
        final int selectDistinctIndex = sql.toLowerCase().indexOf("select distinct");
        return selectIndex + (selectDistinctIndex == selectIndex ? 15 : 6);
    }

    @Override
    public String getLimitString(String sql, int offset, int limit, String orderBy) {
        return getLimit(sql, offset, limit,orderBy);
    }

    /**
     * 将sql变成分页sql语句,提供将offset及limit使用占位符号(placeholder)替换.
     * <pre>
     * 如mysql
     * dialect.getLimitString("select * from user", 12, ":offset",0,":limit") 将返回
     * select * from user limit :offset,:limit
     * </pre>
     *
     * @param sql    实际SQL语句
     * @param offset 分页开始纪录条数
     * @param limit  分页每页显示纪录条数
     * @return 包含占位符的分页sql
     */
    public String getLimit(String sql, int offset, int limit,String orderBy) {
        if (offset >= 0) {
    		int lastForm=sql.lastIndexOf("from");
    		String leftSql=sql.substring(0, lastForm);
    		String rightSql=sql.substring(lastForm);
    		
    		//int orderByIndex=rightSql.lastIndexOf("order by");
    		//String orderby=rightSql.substring(orderByIndex);
    	//	rightSql=rightSql.substring(0, orderByIndex);
    		
    		StringBuilder sqlSbu=new StringBuilder();
    		
    		sqlSbu.append("select * from ( ");
    		sqlSbu.append(leftSql);
    		sqlSbu.append(",ROW_NUMBER() OVER("+orderBy+") AS RowNumber ");
    		sqlSbu.append(rightSql);
    		sqlSbu.append(") as pagetab where RowNumber BETWEEN "+(offset+1)+" and "+(offset+limit)+" order by RowNumber asc");
    		sql=sqlSbu.toString();
        }
        return sql;
    }


}
