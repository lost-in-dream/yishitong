package org.yishitong.common.datapage;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yuefei.Wang
 * @time 2019/5/10 15:56
 * @desc
 */
public class CardsoaPageImpl<T> extends PageImpl<T> {
    private Map<String, Object> map = new HashMap<>();

    public CardsoaPageImpl(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public void putValue(String key, Object value) {
        map.put(key, value);
    }

    public Object getValue(String key) {
        return map.get(key);
    }
}
