package org.yishitong.common.datapage;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.plugin.Interceptor;

/**
 * Mybatis分页拦截器基类
 */
public abstract class BaseInterceptor implements Interceptor, Serializable {

	private static final long serialVersionUID = 1L;

	private String parameterPage;

	public Log log = LogFactory.getLog(this.getClass());

	public Map<String, Dialect> diaMap;
	
	public String defaultDbtype;

	/**
	 * 对参数进行转换和检查
	 * 
	 * @param parameterObject
	 *            参数对象
	 * @param page
	 *            分页对象
	 * @return 分页对象
	 * @throws NoSuchFieldException
	 *             无法找到参数
	 */
	@SuppressWarnings("unchecked")
	public Page<Object> convertParameter(Object parameterObject, Page<Object> page) {
		try {
			if (parameterObject instanceof Page) {
				return (Page<Object>) parameterObject;
			}
			// 支持返回List<Map>
			if (parameterObject instanceof Map) {
				return (Page<Object>) ((Map<String, Object>) parameterObject).get(parameterPage);
			}
			return null;

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 启动是获取配置数据，初始分页数据
	 * 
	 * @param p，配置的数据
	 * 
	 */
	public void initProperties(Properties p) {

		String[] dbTypeList = p.getProperty("dbtype").split(",");
		defaultDbtype=p.getProperty("defaultdbtype");
		if (dbTypeList.length == 0) {
			throw new RuntimeException("设置dbtype,所支持的数据库");
		} else {
			diaMap = new HashMap<String, Dialect>();
		}
		for (String dbType : dbTypeList) {
			if ("mysql".equals(dbType)) {
				diaMap.put(dbType, (new MySQLDialect()));
			} else if ("sqlserver".equals(dbType)) {
				diaMap.put(dbType, (new SQLServerDialect()));
			}
		}

	}
}