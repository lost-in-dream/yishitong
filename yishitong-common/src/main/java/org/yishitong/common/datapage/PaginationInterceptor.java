package org.yishitong.common.datapage;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.yishitong.common.dataSource.DynamicDataSource;
import org.yishitong.common.utils.StringUtil;

import java.util.Properties;

/**
 * 数据库分页插件，只拦截查询语句.
 *
 * @author MaoLiang
 */
@Intercepts({@Signature(type = Executor.class, method = "query",
		args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class PaginationInterceptor extends BaseInterceptor {

	private static final long serialVersionUID = 1L;

	@Override
	public Object intercept(Invocation invocation) throws Throwable {

		//判断是否进行拦截
		if (invocation.getTarget() instanceof Executor) {
			final MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
			DynamicDataSource db = (DynamicDataSource) mappedStatement.getConfiguration().getEnvironment().getDataSource();

			//获取传入参数
			Object parameter = invocation.getArgs()[1];


			//获取分页参数对象
			Page<Object> page = null;
			if (parameter != null && (parameter instanceof Page)) {
				page = (Page<Object>) parameter;
				//重新给传入的参数赋值
				invocation.getArgs()[1] = page.getParameter();
			}

			//判断使用哪种数据库的分页
			Dialect dialect = diaMap.get((db.dataSourceName() == null) ? defaultDbtype : (db.dataSourceName().indexOf("mysql") != -1 ? "mysql" : "sqlserver"));
			//如果设置了分页对象，则进行分页,分页不设置drds
			if (page != null && page.getPageSize() > 0 && page.getSortColumn() != null && page.getSortColumn() != "") {
				BoundSql boundSql = mappedStatement.getBoundSql(page.getParameter());
				if (StringUtil.isBlank(boundSql.getSql())) {
					return null;
				}
				//获取数据库连接，使用数据库产品名称，确定需要进行哪种数据的分页程序
				String originalSql = boundSql.getSql().trim();

				//得到总记录数
				page.setTotal(SQLHelper.getCount(originalSql, null, mappedStatement, page.getParameter(), boundSql, log));
				String pageSql;
				BoundSql newBoundSql;
				if (page.isFy()) {
					//分页查询 本地化对象 修改数据库注意修改实现
					pageSql = SQLHelper.generatePageSql(originalSql, page, dialect);
					invocation.getArgs()[2] = new RowBounds(RowBounds.NO_ROW_OFFSET, RowBounds.NO_ROW_LIMIT);
					newBoundSql = new BoundSql(mappedStatement.getConfiguration(), pageSql, boundSql.getParameterMappings(), boundSql.getParameterObject());
				} else {
					int whIndex = originalSql.lastIndexOf("where");
					if (whIndex == -1) {
						whIndex = originalSql.lastIndexOf("WHERE");
					}
					if (whIndex == -1) {
						newBoundSql = new BoundSql(mappedStatement.getConfiguration(), (originalSql + "where 1=0"), null, null);
					} else {
						newBoundSql = new BoundSql(mappedStatement.getConfiguration(), (originalSql.substring(0, whIndex) + " where 1=0"), null, null);
					}
				}


				MappedStatement newMs = copyFromMappedStatement(mappedStatement, new BoundSqlSqlSource(newBoundSql));

				invocation.getArgs()[0] = newMs;
				page.initialize();//分页页数初始化

			}
		}
		return invocation.proceed();
	}


	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		super.initProperties(properties);
	}

	private MappedStatement copyFromMappedStatement(MappedStatement ms,
	                                                SqlSource newSqlSource) {
		MappedStatement.Builder builder = new MappedStatement.Builder(ms.getConfiguration(),
				ms.getId(), newSqlSource, ms.getSqlCommandType());
		builder.resource(ms.getResource());
		builder.fetchSize(ms.getFetchSize());
		builder.statementType(ms.getStatementType());
		builder.keyGenerator(ms.getKeyGenerator());
		if (ms.getKeyProperties() != null) {
			for (String keyProperty : ms.getKeyProperties()) {
				builder.keyProperty(keyProperty);
			}
		}
		builder.timeout(ms.getTimeout());
		builder.parameterMap(ms.getParameterMap());
		builder.resultMaps(ms.getResultMaps());
		builder.cache(ms.getCache());
		return builder.build();
	}

	public static class BoundSqlSqlSource implements SqlSource {
		BoundSql boundSql;

		public BoundSqlSqlSource(BoundSql boundSql) {
			this.boundSql = boundSql;
		}

		@Override
		public BoundSql getBoundSql(Object parameterObject) {
			return boundSql;
		}
	}
}