package org.yishitong.common.datapage;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页类
 *
 * @param <T>
 * @author plt
 * @version 2013-7-2
 */
public class Page<T> implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private int pageNo = 1;
	private int pageSize;

	private long total;

	@JSONField(serialize = false)
	private int first;

	@JSONField(serialize = false)
	private int last;

	private List<T> rows = new ArrayList<T>();

	public Page() {
		this.pageSize = -1;
	}

	@JSONField(serialize = false)
	private Object parameter;

	@JSONField(serialize = false)
	private String sortColumn;

	private boolean isFy = true;

	/**
	 * 构造方法
	 *
	 * @param pageNo 当前页码
	 */
	public Page(int pageNo) {
		this(pageNo, 10, 0);
	}

	/**
	 * 构造方法
	 *
	 * @param pageNo   当前页码
	 * @param pageSize 分页大小
	 */
	public Page(int pageNo, int pageSize) {
		this(pageNo, pageSize, 0);
	}

	public Page(int pageNo, int pageSize, Object parameter, String sortColumn) {
		this(pageNo, pageSize, 0);
		this.parameter = parameter;
		this.sortColumn = sortColumn;

	}

	public Page(int pageNo, int pageSize, Object parameter) {
		this(pageNo, pageSize, 0);
		this.parameter = parameter;

	}

	/**
	 * 构造方法
	 *
	 * @param pageNo   当前页码
	 * @param pageSize 分页大小
	 * @param count    数据条数
	 */
	public Page(int pageNo, int pageSize, long count) {
		this(pageNo, pageSize, count, new ArrayList<T>());
	}

	/**
	 * 构造方法
	 *
	 * @param pageNo   当前页码
	 * @param pageSize 分页大小
	 * @param count    数据条数
	 * @param list     本页数据对象列表
	 */
	public Page(int pageNo, int pageSize, long count, List<T> list) {
		this.setTotal(count);
		this.setPageNo(pageNo);
		this.pageSize = pageSize;
		this.rows = list;
	}

	/**
	 * 初始化参数
	 */
	public void initialize() {
		// 1
		if (this.pageSize > 0) {
			this.first = 1;
			this.last = (int) (total / (this.pageSize < 1 ? 20 : this.pageSize) + first - 1);
			if (this.total % this.pageSize != 0 || this.last == 0) {
				this.last++;
			}
			if (this.last < this.first) {
				this.last = this.first;
			}
			if (this.pageNo <= 1) {
				this.pageNo = this.first;
			}
			if (this.pageNo >= this.last) {
				this.pageNo = this.last;
			}
			// 2
			if (this.pageNo < this.first) {
				this.pageNo = this.first;
			}
			if (this.pageNo > this.last) {
				this.pageNo = this.last;
			}
		}
	}

	/**
	 * 获取设置总数
	 *
	 * @return
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * 设置数据总数
	 *
	 * @param count
	 */
	public void setTotal(long count) {
		this.total = count;

		//上一页是否大于总页数,不需要进行分页查询
		if (pageSize > 0 && ((pageNo - 1) * pageSize) >= count) {
			isFy = false;
		} else {
			isFy = true;
		}
		this.initialize();
	}

	/**
	 * 获取当前页码
	 *
	 * @return
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * 设置当前页码
	 *
	 * @param pageNo
	 */
	public void setPageNo(int pageNo) {
		if (pageNo <= 0) {
			pageNo = 1;
		}
		this.pageNo = pageNo;
	}

	/**
	 * 获取页面大小
	 *
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页面大小（最大500）
	 *
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize <= 0 ? 10 : pageSize;
	}

	/**
	 * 首页索引
	 *
	 * @return
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * 尾页索引
	 *
	 * @return
	 */
	public int getLast() {
		return last;
	}

	/**
	 * 获取页面总数
	 *
	 * @return getLast();
	 */
	public int getTotalPage() {
		return getLast();
	}

	/**
	 * 获取本页数据对象列表
	 *
	 * @return List<T>
	 */
	public List<T> getRows() {
		return rows;
	}

	/**
	 * 设置本页数据对象列表
	 *
	 * @param list
	 */
	public void setRows(List<T> list) {
		this.rows = list;

	}

	/**
	 * 获取 Hibernate FirstResult
	 */
	public int FirstResult() {
		int firstResult = (getPageNo() - 1) * getPageSize();
		if (firstResult >= getTotal()) {
			firstResult = 0;
		}
		return firstResult;
	}

	/**
	 * 获取 Hibernate MaxResults
	 */
	public int MaxResults() {
		return getPageSize();
	}

	public Object getParameter() {
		return parameter;
	}

	public void setParameter(Object parameter) {
		this.parameter = parameter;

	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public boolean isFy() {
		return isFy;
	}

	public void setFy(boolean isFy) {
		this.isFy = isFy;
	}

}