package org.yishitong.common.datapage;

/**
 * 分页接口
 *
 * @author plt
 */
public interface Dialect {

	/**
	 * 数据库本身是否支持分页当前的分页查询方式
	 * 如果数据库不支持的话，则不进行数据库分页
	 *
	 * @return true：支持当前的分页查询方式
	 */
	boolean supportsLimit();

	/**
	 * 将sql转换为分页SQL，分别调用分页sql
	 *
	 * @param sql    SQL语句
	 * @param offset 开始条数
	 * @param limit  每页显示多少纪录条数
	 * @param orderBy
	 * @return 分页查询的sql
	 */
	String getLimitString(String sql, int offset, int limit, String orderBy);

	/**
	 * lll
	 * @return
	 */
	default boolean isDrds() {
		return false;
	}
}