package org.yishitong.common.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ZmtGoods implements Serializable {
    private static final long serialVersionUID = -45084951913369338L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 运营商id
     */
    private Integer operatorId;
    /**
     * 品牌id
     */
    private Integer brandId;
    /**
     * 型号id
     */
    private Integer modelId;
    /**
     * 产品类别1-机具2礼包
     */
    private String goodsType;
    /**
     * 产品名称
     */
    private String goodsName;
    /**
     * 包含数量
     */
    private String goodsCount;
    /**
     * 产品金额
     */
    private Double goodsAmt;
    /**
     * 产品积分
     */
    private String goodsPoint;
    /**
     * 产品图片
     */
    private String goodsImg;
    /**
     * 商品详情
     */
    private String goodsDetails;
    /**
     * 是否购买有返佣(0否1是)
     */
    private String isRebate;
    /**
     * 上级返佣
     */
    private BigDecimal oneRebate;
    /**
     * 上上级返佣
     */
    private BigDecimal twoRebate;
    /**
     * 上上上级返佣
     */
    private BigDecimal threeRebate;
    /**
     * 产品状态0下架1上架
     */
    private String goodsStat;
    /**
     * 产品说明
     */
    private String goodsRecord;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtModified;
    /**
     * 是否前端显示1是0否
     */
    private String isDisplay;
    /**
     * 返佣详情
     */
    private String rebateDetails;
    /**
     * 详情图片
     */
    private String goodsDetailsImg;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(String goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Double getGoodsAmt() {
        return goodsAmt;
    }

    public void setGoodsAmt(Double goodsAmt) {
        this.goodsAmt = goodsAmt;
    }

    public String getGoodsPoint() {
        return goodsPoint;
    }

    public void setGoodsPoint(String goodsPoint) {
        this.goodsPoint = goodsPoint;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getGoodsDetails() {
        return goodsDetails;
    }

    public void setGoodsDetails(String goodsDetails) {
        this.goodsDetails = goodsDetails;
    }

    public String getIsRebate() {
        return isRebate;
    }

    public void setIsRebate(String isRebate) {
        this.isRebate = isRebate;
    }



    public String getGoodsStat() {
        return goodsStat;
    }

    public void setGoodsStat(String goodsStat) {
        this.goodsStat = goodsStat;
    }

    public String getGoodsRecord() {
        return goodsRecord;
    }

    public void setGoodsRecord(String goodsRecord) {
        this.goodsRecord = goodsRecord;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(String isDisplay) {
        this.isDisplay = isDisplay;
    }

    public String getRebateDetails() {
        return rebateDetails;
    }

    public void setRebateDetails(String rebateDetails) {
        this.rebateDetails = rebateDetails;
    }

    public String getGoodsDetailsImg() {
        return goodsDetailsImg;
    }

    public void setGoodsDetailsImg(String goodsDetailsImg) {
        this.goodsDetailsImg = goodsDetailsImg;
    }

    public BigDecimal getOneRebate() {
        return oneRebate;
    }

    public void setOneRebate(BigDecimal oneRebate) {
        this.oneRebate = oneRebate;
    }

    public BigDecimal getTwoRebate() {
        return twoRebate;
    }

    public void setTwoRebate(BigDecimal twoRebate) {
        this.twoRebate = twoRebate;
    }

    public BigDecimal getThreeRebate() {
        return threeRebate;
    }

    public void setThreeRebate(BigDecimal threeRebate) {
        this.threeRebate = threeRebate;
    }
}

