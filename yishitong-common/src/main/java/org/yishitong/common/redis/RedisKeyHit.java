package org.yishitong.common.redis;

import java.util.UUID;
import org.yishitong.common.utils.CommonConstant;

public class RedisKeyHit {

    /**
     * 碰一碰登录次数超限锁定
     */
    public static String getLoginNumLock(String operatorId, String mobile) {
        return "hit:loginNum:" + operatorId + ":" + ":" + mobile;
    }

    /**
     * 生成碰一碰token
     */
    public static String generateToken() {
        String UUIDString = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        return CommonConstant.HIT_TOKEN_PREFIX + UUIDString;
    }
}
