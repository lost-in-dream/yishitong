package org.yishitong.common.redis;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class RedisSpringUtil {

    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 设置 redis key为任意对象
     *
     * @param key
     * @param value
     */
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value, 7200, TimeUnit.MINUTES);

    }

    /**
     * 设置 redis key为任意对象
     *
     * @param key
     * @param value
     * @param timeOut 超时时间 分钟
     */
    public void set(String key, Object value, long timeOut) {
        redisTemplate.opsForValue().set(key, value, timeOut, TimeUnit.MINUTES);
    }

    /**
     * 获取redis key的任意对象
     *
     * @param key
     * @return
     */
    public <T> T get(String key) {
        return (T) redisTemplate.opsForValue().get(key);
    }

    /**
     * 获取redis key的任意对象
     *
     * @param key
     * @return
     */
    private static String redisCode = "utf-8";

    public String getStr(String key) {
        return (String) redisTemplate.execute((RedisCallback<Object>) connection -> {
            try {
                byte[] bt = connection.get(key.getBytes());
                if (bt == null) {
                    return null;
                }
                return new String(bt, redisCode);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return "";
        });
    }

    /**
     * @param key
     * @param value
     * @param liveTime
     */
    public void setStr(final byte[] key, final byte[] value, final long liveTime) {
        redisTemplate.execute((RedisCallback) connection -> {
            connection.set(key, value);
            if (liveTime > 0) {
                connection.expire(key, liveTime);
            }
            return 1L;
        });
    }

    public void setStr(String key, String value) {
        try {
            this.setStr(key.getBytes("utf-8"), value.getBytes("utf-8"), 7200 * 60);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置set
     *
     * @param key
     * @param value
     */
    public void setSet(String key, Object value) {
        redisTemplate.opsForSet().add(key, value);
    }

    public boolean inSet(String key, Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 弹出一个set值
     *
     * @param key
     * @return
     */
    public Object popSet(String key) {
        return redisTemplate.opsForSet().pop(key);
    }

    /**
     * 获取全部set数据
     *
     * @param key
     * @return
     */
    public Set<Object> allSet(String key) {
        return redisTemplate.opsForSet().members(key);
    }


    /**
     * 设置map
     *
     * @param key
     * @param mp
     */
    public void setMap(String key, Map<String, Object> mp) {
        redisTemplate.opsForHash().putAll(key, mp);
    }

    /**
     * 设置map
     *
     * @param key
     * @param hashKey
     * @param value
     */
    public void setMap(String key, String hashKey, Object value) {
        redisTemplate.opsForHash().put(key, hashKey, value);
    }

    /**
     * 获取map某个key的值
     *
     * @param key
     * @param hashKey
     */
    public Object getMap(String key, String hashKey) {

        return redisTemplate.opsForHash().get(key, hashKey);
    }

    /**
     * 不可用
     *
     * @param key
     * @param patternKey
     * @return
     * @throws IOException
     */
    public Map scanMap(String key, String patternKey) throws IOException {
        ScanOptions build = ScanOptions.scanOptions().count(2000).match(patternKey).build();
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(key, build);

        Map<String, String> resMap = new HashMap<>(1024);
        while (cursor.hasNext()) {
            String keyIn = (String) cursor.next().getKey();
            String value = String.valueOf(cursor.next().getValue());

            resMap.put(keyIn, value);
        }
        cursor.close();

        return resMap;
    }

    /**
     * 获取map某个key是否存在
     *
     * @param key
     * @param hashKey
     */
    public boolean isKeyMap(String key, String hashKey) {

        return redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * 获取redis key的Map
     *
     * @param key
     * @return
     */
    public Map<Object, Object> getMap(String key) {

        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 判断key是否存在
     *
     * @param key
     * @return
     */
    public boolean isKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public Boolean deleteKey(String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 设置key时间
     *
     * @param key
     * @param timeOut
     */
    public void setKeyTime(String key, long timeOut) {
        redisTemplate.expire(key, timeOut, TimeUnit.MINUTES);
    }

    public List<Object> getList(String key) {

        return redisTemplate.opsForList().range(key, 0, -1);
    }

    public long setList(String key, String value) {

        return redisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 设置Map
     *
     * @param key
     * @param field
     * @param value
     * @param liveTime
     */
    public void setMapStr(String key, String field, String value, long liveTime) {
        redisTemplate.execute((RedisCallback) connection -> {
            connection.hSetNX(key.getBytes(), field.getBytes(), value.getBytes());
            if (liveTime > 0) {
                connection.expire(key.getBytes(), liveTime);
            }
            return 1L;
        });
    }

    public void setStrInfinite(String key, String value) {
        try {
            this.setStr(key.getBytes("utf-8"), value.getBytes("utf-8"), -1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置 redis key为任意对象
     *
     * @param key
     * @param value
     */
    public void setMaxDate(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);

    }


    /**
     * 自增double类型
     *
     * @param key
     * @param value
     * @return
     */
    public Double incrDecimal(String key, BigDecimal value) {
        //不传时间默认5天
        return incrDecimal(key, value, 7200L);
    }

    /**
     * 带时间设置自增项
     *
     * @param key
     * @param value
     * @param timeout
     * @return
     */
    public Double incrDecimal(String key, BigDecimal value, Long timeout) {
        Double inl = redisTemplate.opsForValue().increment(key, value.doubleValue());
        if (new BigDecimal(String.valueOf(inl)).compareTo(value) == 0) {
            redisTemplate.expire(key, timeout, TimeUnit.MINUTES);
        }
        return inl;
    }


    /**
     * 自增long类型
     *
     * @param key
     * @param value
     * @return
     */
    public Long incrLong(String key, Long value) {
        return incrLong(key, value, 7200L);
    }

    /**
     * 设置 自增项
     *
     * @param key
     * @param value
     */
    public Long incrLong(String key, Long value, Long timeout) {
        Long inl = redisTemplate.opsForValue().increment(key, value);
        if (inl.longValue() == value.longValue()) {
            redisTemplate.expire(key, timeout, TimeUnit.MINUTES);
        }
        return inl;
    }

    /**
     * 自增double类型
     *
     * @param key
     * @param value
     * @return
     */
    public Double incrDecimalHash(String hkey, String key, BigDecimal value) {
        //不传时间默认5天
        return incrDecimalHash(hkey, key, value, 7200L);
    }

    /**
     * 带时间设置自增项
     *
     * @param key
     * @param value
     * @param timeout
     * @return
     */
    public Double incrDecimalHash(String hkey, String key, BigDecimal value, Long timeout) {
        Double inl = redisTemplate.opsForHash().increment(hkey, key, value.doubleValue());
        if (new BigDecimal(String.valueOf(inl)).compareTo(value) == 0) {
            redisTemplate.expire(hkey, timeout, TimeUnit.MINUTES);
        }
        return inl;
    }


    /**
     * 自增long类型
     *
     * @param key
     * @param value
     * @return
     */
    public Long incrLongHash(String hkey, String key, Long value) {
        return incrLongHash(hkey, key, value, 7200L);
    }

    /**
     * 设置 自增项
     *
     * @param key
     * @param value
     */
    public Long incrLongHash(String hkey, String key, Long value, Long timeout) {
        Long inl = redisTemplate.opsForHash().increment(hkey, key, value);
        if (inl.longValue() == value.longValue()) {
            redisTemplate.expire(hkey, timeout, TimeUnit.MINUTES);
        }
        return inl;
    }

    /**
     * 设置自增键
     * 截至到某个时间戳 过期
     *
     * @param key
     * @param value
     * @param date
     * @return
     */
    public Long incrLong(String key, Long value, Date date) {
        Long inl = redisTemplate.opsForValue().increment(key, value);
        if (inl == 1) {
            redisTemplate.expireAt(key, date);
        }
        return inl;
    }

    /**
     * 获取计数器的数量
     *
     * @param key
     * @return
     */
    public Long getIncrLongValue(String key) {
        return Long.valueOf(redisTemplate.boundValueOps(key).get(0, -1));
    }

    /**
     * 设置string 带时间的
     *
     * @param key
     * @param value
     * @param liveTime
     */
    public void setStrTime(String key, String value, Long liveTime) {
        try {
            this.setStr(key.getBytes("utf-8"), value.getBytes("utf-8"), liveTime * 60);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 批量删除key
     *
     * @param lKey
     */
    public void deleteListKey(List<String> lKey) {
        redisTemplate.delete(lKey);
    }

    /**
     * @param key
     * @param value
     * @param liveTime
     */
    public void setStrNx(String key, String value, long liveTime) {
        redisTemplate.execute((RedisCallback) connection -> {
            try {
                connection.setNX(key.getBytes("utf-8"), value.getBytes("utf-8"));
                if (liveTime > 0) {
                    connection.expire(key.getBytes("utf-8"), liveTime);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return 1L;
        });
    }

    /**
     * 尝试获取分布式锁
     *
     * @param lockKey    锁
     * @param requestId  请求标识
     * @param expireTime 超期时间
     * @param isCluster  是否集群模式
     * @return 是否获取成功
     * @maxRetryTimes 重试次数
     */
    public boolean tryGetDistributedLock(String lockKey, String requestId, Long expireTime, int maxRetryTimes, boolean isCluster) {

        for (int i = 0; i < maxRetryTimes; i++) {
            String status = redisTemplate.execute((RedisCallback<String>) connection -> {
                String result;
                if (isCluster) {
                    JedisCluster jedisCluster = (JedisCluster) connection.getNativeConnection();
                    result = jedisCluster.set(lockKey, requestId, "nx", "ex", expireTime);
                } else {
                    Jedis jedis = (Jedis) connection.getNativeConnection();
                    result = jedis.set(lockKey, requestId, "nx", "ex", expireTime);
                }
                return result;
            });
            // 抢到锁
            if ("OK".equals(status)) {
                return true;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                break;
            }

        }

        return false;

    }

    /**
     * 释放分布式锁
     *
     * @param lockKey   锁
     * @param requestId 请求标识
     * @param isCluster 是否集群模式
     * @return 是否释放成功
     */
    public boolean releaseDistributedLock(String lockKey, String requestId, boolean isCluster) {

        String status = redisTemplate.execute((RedisCallback<String>) connection -> {
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            Object result;
            if (isCluster) {
                JedisCluster jedisCluster = (JedisCluster) connection.getNativeConnection();
                result = jedisCluster.eval(script, Collections.singletonList(lockKey),
                        Collections.singletonList(requestId));
            } else {
                Jedis jedis = (Jedis) connection.getNativeConnection();
                result = jedis.eval(script, Collections.singletonList(lockKey),
                        Collections.singletonList(requestId));
            }
            return ((result.equals(1)) ? "OK" : "");

        });
        if ("OK".equals(status)) {
            return true;
        }

        return false;

    }

    public void zsetAdd(String key, Object value, Double weight) {
        redisTemplate.opsForZSet().add(key, value, weight);
    }

    public Long zsetReverseRankAddOne(String key, Object value) {
        Long rank = redisTemplate.opsForZSet().reverseRank(key, value);
        if (null == rank) {
            return 0L;
        }
        return ++rank;
    }

    public void zsetRemoveAll(String key) {
        redisTemplate.opsForZSet().removeRange(key, 0, 99999);
    }

    /**
     * 订单是否已存在
     * @param operatorId 运营商ID
     * @param brandCode 品牌编码
     * @param orderId 订单号
     * @return true 存在，false 不存在
     */
    public boolean orderRepeatExist(Integer operatorId, String brandCode, String orderId) {
        return null != redisTemplate.opsForZSet().rank(RedisKey.getOrderRepeatSetKey(operatorId, brandCode), orderId);
    }

    /**
     * 订单是否已存在
     * @param operatorId 运营商ID
     * @param brandCode 品牌编码
     * @param otherSign 其他标识
     * @param orderId 订单号
     * @return true 存在，false 不存在
     */
    public boolean orderRepeatExist(Integer operatorId, String brandCode, String otherSign, String orderId) {
        return null != redisTemplate.opsForZSet().rank(RedisKey.getOrderRepeatSetKey(operatorId, brandCode, otherSign), orderId);
    }

    /**
     * 订单放入缓存
     * @param operatorId 运营商ID
     * @param brandCode 品牌编码
     * @param orderId 订单号
     */
    public void orderRepeatAdd(Integer operatorId, String brandCode, String orderId) {
        String orderSetKey = RedisKey.getOrderRepeatSetKey(operatorId, brandCode);
        redisTemplate.opsForZSet().add(orderSetKey, orderId,
                NumberUtils.toDouble(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
        if (RandomUtils.nextInt(200) == 0) {
            redisTemplate.opsForZSet().removeRangeByScore(orderSetKey, 0D,
                    NumberUtils.toDouble(LocalDate.now().minusDays(5).format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
        }
    }

    /**
     * 订单放入缓存
     * @param operatorId 运营商ID
     * @param brandCode 品牌编码
     * @param otherSign 其他标识
     * @param orderId 订单号
     */
    public void orderRepeatAdd(Integer operatorId, String brandCode, String otherSign, String orderId) {
        String orderSetKey = RedisKey.getOrderRepeatSetKey(operatorId, brandCode, otherSign);
        redisTemplate.opsForZSet().add(orderSetKey, orderId,
                NumberUtils.toDouble(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
        if (RandomUtils.nextInt(200) == 0) {
            redisTemplate.opsForZSet().removeRangeByScore(orderSetKey, 0D,
                    NumberUtils.toDouble(LocalDate.now().minusDays(5).format(DateTimeFormatter.ofPattern("yyyyMMdd"))));
        }
    }
}
