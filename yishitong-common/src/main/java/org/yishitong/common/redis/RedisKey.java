package org.yishitong.common.redis;

import org.apache.commons.lang3.time.FastDateFormat;
import org.yishitong.common.utils.CommonConstant;

import java.util.Date;
import java.util.UUID;

public class RedisKey {

    public static String PAY_AGENT_COUNT = "statis:pay:count:agent:%s:%s:%s:%s";
    public static String PAY_AGENT_SUM_AMOUNT = "statis:pay:amount:agent:%s:%s:%s:%s";
    public static String REBATE_AGENT_COUNT = "statis:rebate:count:agent:%s:%s:%s:%s";
    public static String REBATE_AGENT_SUM_AMOUNT = "statis:rebate:amount:agent:%s:%s:%s:%s";

    public static String PAY_CHANNEL_COUNT = "statis:pay:count:channel:%s:%s:%s";
    public static String PAY_CHANNEL_SUM_AMOUNT = "statis:pay:amount:channel:%s:%s:%s";
    public static String REBATE_CHANNEL_COUNT = "statis:rebate:count:channel:%s:%s:%s";
    public static String REBATE_CHANNEL_SUM_AMOUNT = "statis:rebate:amount:channel:%s:%s:%s";

    public static String PAY_ACTIVE_AGENT_SET = "statis:pay:agent:active:%s:%s";
    public static String REBATE_ACTIVE_AGENT_SET = "statis:rebate:agent:active:%s:%s";
    /**
     * 模糊匹配  同类型包含count的存在amount一定存在
     * 如："statis:pay:count:channel:*" 存在"statis:pay:amount:channel:*" 一定存在
     */
    public static String PAY_CHANNEL_COUNT_KEY = "statis:pay:count:channel:";
    public static String PAY_AGENT_COUNT_KEY = "statis:pay:count:agent:";
    public static String REBATE_AGENT_COUNT_KEY = "statis:rebate:count:agent:";
    public static String REBATE_CHANNEL_COUNT_KEY = "statis:rebate:count:channel:";

    public static String SENSITIVE_WORDS = "sensitiveWords";
    public static String REAL_POST_PREFIX = "realPost_";
    public static String STATISTIC_ACCUMULATE = "statisticAccumulate_";
    public static String STATISTIC_HOME = "statisticHome_";
    public static String OPERATOR_ROLE_MENU = "admin:operator:role:%s:%s";
    public static String OPERATOR_ROLE_STAFF = "admin:operator:staff:%s:%s";
    public static String OPERATOR_ROLE_API = "admin:operator:role:api:%s:%s";
    public static String OPERATOR_MENU = "admin:operator:menu:%s:%s";
    public static String OPERATOR_MENU_ISSUPER = "admin:operator:menu:issuper:%s:%s";
    public static String OPERATOR_API_ISSUPER = "admin:operator:api:issuper:%s:%s";
    public static String OPERATOR_ISSUPER = "admin:operator:issuper:%s:%s";
    public static String OPERATOR_API_SUPER = "admin:operator:api:super:%s:%s";

    public static String SMSCODE_APP_WITHDRAW = "app_withdraw";
    public static String REALPOST_COMPLETE_INFO = "realpostComplete_";

    public static String PAY_AGENT_BRAND_CHANNEL_COUNT = "statis:pay:count:agent:brand:channel:%s:%s:%s:%s:%s:%s";
    public static String PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT = "statis:pay:amount:agent:brand:channel:%s:%s:%s:%s:%s:%s";
    public static String REBATE_AGENT_BRAND_CHANNEL_COUNT = "statis:rebate:count:agent:brand:channel:%s:%s:%s:%s:%s:%s";
    public static String REBATE_AGENT_BRAND_CHANNEL_SUM_AMOUNT = "statis:rebate:amount:agent:brand:channel:%s:%s:%s:%s:%s:%s";
    public static String PAY_AGENT_BRAND_CHANNEL_COUNT_SEARCH = "statis:pay:count:agent:brand:channel:";
    public static String PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT_SEARCH = "statis:pay:amount:agent:brand:channel:";
    public static String REBATE_AGENT_BRAND_CHANNEL_COUNT_SEARCH = "statis:rebate:count:agent:brand:channel:";
    public static String REBATE_AGENT_BRAND_CHANNEL_SUM_AMOUNT_SEARCH = "statis:rebate:amount:agent:brand:channel:";

    public static String PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT_INDIRECT = "statis:pay:amount:agent:brand:channel:%s:%s:%s";
    public static String OPERATOR_BRAND_FEE = "admin:operator:brand:fee:%s:%s:%s";
    public static FastDateFormat dateFormat = FastDateFormat.getInstance("yyyyMMdd");
    public static int AGENT_BRAND_CHANNEL_SPLIT = 12;

    /**
     * 登录获取token
     *
     * @return
     */
    public static String[] getTokenKey() {
        return getkey("tokenId");
    }

    /**
     * 碰一碰登录获取token
     *
     * @return
     */
    public static String[] getPpTokenKey() {
        return getkey("pPtokenId");
    }

    public static String getTokenKey(String kv) {
        return ("tokenId:" + kv);
    }

    private static String[] getkey(String keyTop) {
        String rkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        String keyData = keyTop + ":" + rkey;
        return new String[]{rkey, keyData};
    }

    public static String[] getSmsCodeKey(String mobile) {
        return getkey(mobile);
    }

    public static String getSmsCodeKey(String mobile, String key) {
        return mobile + ":" + key;
    }

    /**
     * 登录密码错误锁定
     *
     * @param mobile
     * @return
     */
    public static String getLoginErrLock(String operatorId, String mobile) {
        return "loginLock:" + operatorId + ":" + mobile;
    }

    /**
     * 闪验使用次数限制key
     *
     * @param operatorId
     * @param mobile
     * @return
     */
    public static String flashLimit(String operatorId, String mobile) {
        return "flashLimit:" + operatorId + "_" + mobile;
    }

    /**
     * 登录次数超限锁定
     *
     * @param mobile
     * @return
     */
    public static String getLoginNumLock(String operatorId, String appId, String mobile) {
        return "loginNum:" + operatorId + ":" + appId + ":" + mobile;
    }

    public static String getSuper() {
        return ("Super");
    }


    /**
     * 代理商直属交易数量
     *
     * @param operatorId
     * @param agentId
     * @return
     */
    public static String getPayAgentCount(Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(PAY_AGENT_COUNT, getFormatDate(), operatorId, agentLevel, agentId);
    }

    public static String getPayAgentCount(String date, Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(PAY_AGENT_COUNT, date, operatorId, agentLevel, agentId);
    }

    /**
     * 代理商直属交易金额总和
     *
     * @param operatorId
     * @param agentId
     * @return
     */
    public static String getPayAgentSumAmount(Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(PAY_AGENT_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel, agentId);
    }

    public static String getPayAgentSumAmountMultiKey(Integer operatorId, String agentLevel, Integer agentId) {
        String tempKey = String.format(PAY_AGENT_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel + agentId + "*", agentId);
        return tempKey.substring(0, tempKey.lastIndexOf(":"));
    }

    public static String getPayAgentSumAmount(String date, Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(PAY_AGENT_SUM_AMOUNT, date, operatorId, agentLevel, agentId);
    }

    /**
     * 代理商受益交易返佣笔数
     *
     * @param operatorId
     * @param agentId
     * @return
     */
    public static String getRebateAgentCount(Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(REBATE_AGENT_COUNT, getFormatDate(), operatorId, agentLevel, agentId);
    }

    public static String getRebateAgentCount(String date, Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(REBATE_AGENT_COUNT, date, operatorId, agentLevel, agentId);
    }

    /**
     * 代理商受益交易返佣总金额
     *
     * @param operatorId
     * @param agentId
     * @return
     */
    public static String getRebateAgentSumAmount(Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(REBATE_AGENT_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel, agentId);
    }

    public static String getRebateAgentSumAmount(String date, Integer operatorId, String agentLevel, Integer agentId) {
        return String.format(REBATE_AGENT_SUM_AMOUNT, date, operatorId, agentLevel, agentId);
    }

    /**
     * 渠道交易数量
     *
     * @param operatorId
     * @param channelId
     * @return
     */
    public static String getPayChannelCount(Integer operatorId, Integer channelId) {
        return String.format(PAY_CHANNEL_COUNT, getFormatDate(), operatorId, channelId);
    }

    public static String getPayChannelCount(String date, Integer operatorId, Integer channelId) {
        return String.format(PAY_CHANNEL_COUNT, date, operatorId, channelId);
    }

    /**
     * 渠道交易总金额
     *
     * @param operatorId
     * @param channelId
     * @return
     */
    public static String getPayChannelSumAmount(Integer operatorId, Integer channelId) {
        return String.format(PAY_CHANNEL_SUM_AMOUNT, getFormatDate(), operatorId, channelId);
    }

    public static String getPayChannelSumAmount(String date, Integer operatorId, Integer channelId) {
        return String.format(PAY_CHANNEL_SUM_AMOUNT, date, operatorId, channelId);
    }

    /**
     * 渠道返佣总笔数
     *
     * @param operatorId
     * @param channelId
     * @return
     */
    public static String getRebateChannelCount(Integer operatorId, Integer channelId) {
        return String.format(REBATE_CHANNEL_COUNT, getFormatDate(), operatorId, channelId);
    }

    public static String getRebateChannelCount(String date, Integer operatorId, Integer channelId) {
        return String.format(REBATE_CHANNEL_COUNT, date, operatorId, channelId);
    }

    /**
     * 渠道返佣总金额
     *
     * @param operatorId
     * @param channelId
     * @return
     */
    public static String getRebateChannelSumAmount(Integer operatorId, Integer channelId) {
        return String.format(REBATE_CHANNEL_SUM_AMOUNT, getFormatDate(), operatorId, channelId);
    }

    public static String getRebateChannelSumAmount(String date, Integer operatorId, Integer channelId) {
        return String.format(REBATE_CHANNEL_SUM_AMOUNT, date, operatorId, channelId);
    }


    /**
     * 活跃代理商集合-交易
     *
     * @return
     */
    public static String getPayActiveAgentSet(Integer operatorId) {
        return String.format(PAY_ACTIVE_AGENT_SET, getFormatDate(), operatorId);
    }

    public static String getPayActiveAgentSet(String date, Integer operatorId) {
        return String.format(PAY_ACTIVE_AGENT_SET, date, operatorId);
    }

    /**
     * 活跃代理商集合-返佣
     *
     * @return
     */
    public static String getRebateActiveAgentSet(Integer operatorId) {
        return String.format(REBATE_ACTIVE_AGENT_SET, getFormatDate(), operatorId);
    }

    public static String getRebateActiveAgentSet(String date, Integer operatorId) {
        return String.format(REBATE_ACTIVE_AGENT_SET, date, operatorId);
    }

    /**
     * 根据角色获取所有的页面
     *
     * @param operatorId
     * @param roleId
     * @return
     */
    public static String getRoleMenu(Integer operatorId, Integer roleId) {
        return String.format(OPERATOR_ROLE_MENU, operatorId, roleId);
    }

    public static String getRoleMenu2(Integer operatorId, Integer staffId) {
        return String.format(OPERATOR_ROLE_STAFF, operatorId, staffId);
    }

    /**
     * 根据角色获取所有的api
     *
     * @param operatorId
     * @param roleId
     * @return
     */
    public static String getRoleApi(Integer operatorId, String roleId) {
        return String.format(OPERATOR_ROLE_API, operatorId, roleId);
    }

    /**
     * 获取超管的api
     *
     * @param operatorId
     * @param staffId
     * @return
     */
    public static String getRoleApiIsSupper(Integer operatorId, Integer staffId) {
        return String.format(OPERATOR_API_ISSUPER, operatorId, staffId);
    }

    /**
     * 获取超超超管的api
     *
     * @param operatorId
     * @param staffId
     * @return
     */
    public static String getApiIsSupper(Integer operatorId, Integer staffId) {
        return String.format(OPERATOR_API_SUPER, operatorId, staffId);
    }

    /**
     * 获取所有有用的页面
     *
     * @param operatorId
     * @param roleId
     * @return
     */
    public static String getOperatorMenu(Integer operatorId, Integer roleId) {
        return String.format(OPERATOR_MENU, operatorId, roleId);
    }

    /**
     * 获取超管的页面
     *
     * @param operatorId
     * @param staffId
     * @return
     */
    public static String getOperatorMenuIsSupper(Integer operatorId, Integer staffId) {
        return String.format(OPERATOR_MENU_ISSUPER, operatorId, staffId);
    }

    /**
     * 获取超超超管的页面
     *
     * @param operatorId
     * @param staffId
     * @return
     */
    public static String getOperatorIsSupper(Integer operatorId, Integer staffId) {
        return String.format(OPERATOR_ISSUPER, operatorId, staffId);
    }

    /**
     * 获取当前日期格式化字符串
     *
     * @return yyyy-MM-dd
     */
    public static String getFormatDate() {
        return dateFormat.format(new Date());
    }

    /**
     * 获取三要素验证失败次数
     */
    public static String getRealPostFailedTimes(String cardNo) {
        return REAL_POST_PREFIX + cardNo;
    }

    /**
     * 获取敏感词字典
     */
    public static String getSensitiveWords() {
        return SENSITIVE_WORDS;
    }

    /**
     * 图形验证码
     */
    public static String[] getImgKey() {
        return getkey("img");
    }

    public static String getStatisticAccumulateKeySign(Integer agentId) {
        return STATISTIC_ACCUMULATE + agentId + "_sign";
    }

    public static String getStatisticAccumulateKey(Integer agentId, String brandId) {
        return STATISTIC_ACCUMULATE + agentId + "_" + brandId;
    }

    public static String getRealPostCompleteInfoKey(String completeKey) {
        return REALPOST_COMPLETE_INFO + completeKey;
    }

    public static String getSyMposLast(Integer operatorId, Integer brandId) {
        return "LASTSUCCESSTIME:SYMPOS:" + operatorId + ":" + brandId;
    }

    public static String getSyMposActLast(Integer operatorId, Integer brandId) {
        return "LASTSUCCESSTIME:SYMPOSACT:" + operatorId + ":" + brandId;
    }

    public static String getSyMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "SYMPOSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getYLFMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "YLFMPOSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getLDYSMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "LDYSMPOSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getFortuneBillMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "FORTUNEBILLMPOSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getLeShuaMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "LESHUAMPOSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getSyMposAct(Integer operatorId, Integer brandId, String snCode) {
        return "SYMPOSACT:" + operatorId + ":" + brandId + ":" + snCode;
    }

    public static String getZfTransLast(Integer operatorId) {
        return "LASTSUCCESSTIME:ZFTRANS:" + operatorId;
    }

    public static String getZfTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ZFTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getTftTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "TFTTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getLdTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "LDTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getTftTransMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "TFTTRANSMPOSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getKqTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "KQTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getHkTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "HKTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    /**
     * 喔刷交易流水号Key
     *
     * @return
     */
    public static String getWoshuaOrder(Integer operatorId, Integer brandId, String transNum) {
        return "WOSHUA:" + operatorId + ":" + brandId + ":" + transNum;
    }

    public static String getXiaoBaoOrder(Integer operatorId, Integer brandId, String transNum) {
        return "XIAOBAO:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 杉德交易流水号Key
     *
     * @return
     */
    public static String getSandpayOrder(Integer operatorId, Integer brandId, String transNum) {
        return "SANDPAY:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 乐刷卡乐宝交易流水号Key
     *
     * @return
     */
    public static String getLeshuaKlbOrder(Integer operatorId, Integer brandId, String transNum) {
        return "LESHUAKLB:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 鑫火乐刷交易流水号Key
     *
     * @return
     */
    public static String getLeshuaXhOrder(Integer operatorId, Integer brandId, String transNum) {
        return "LESHUAXh:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 鑫火乐刷电签交易流水号Key
     *
     * @return
     */
    public static String getLeshuaXhdqOrder(Integer operatorId, Integer brandId, String transNum) {
        return "LESHUAXHDQ:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 趣付交易流水号Key
     *
     * @return
     */
    public static String getQupayOrder(Integer operatorId, Integer brandId, String transNum) {
        return "QUPAY:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 拉卡拉交易流水号Key
     *
     * @return
     */
    public static String getLakalapayOrder(Integer operatorId, Integer brandId, String transNum) {
        return "LAKALAPAY:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 鑫考拉交易流水号Key
     *
     * @return
     */
    public static String getXinkaolaOrder(Integer operatorId, Integer brandId, String transNum) {
        return "XINKAOLA:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 付惠吧交易流水号Key
     * @return
     */
    public static String getFuhuipayOrder(Integer operatorId, Integer brandId, String transNum) {
        return "FUHUIPAY:" + operatorId + ":" + brandId + ":" + transNum;
    }

    /**
     * 通易付交易流水号Key
     *
     * @return
     */
    public static String getTyfOrder(Integer operatorId, Integer brandId, String transNum) {
        return "TYF:" + operatorId + ":" + brandId + ":" + transNum;
    }

    public static String getHkActivateOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "HKACTIVTEORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getLdActivateOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "LDACTIVTEORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getSdActivateOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "SDACTIVTEORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getZfActivateLast(Integer operatorId) {
        return "LASTSUCCESSTIME:ZFACTIVATE:" + operatorId;
    }

    public static String getZfActivate(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ZFACTIVATE:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getTftActivate(Integer operatorId, Integer brandId, String tradeSerial) {
        return "TFTACTIVATE:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getTftActivateMpos(Integer operatorId, Integer brandId, String tradeSerial) {
        return "TFTACTIVATEMPOS:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getKqActivate(Integer operatorId, Integer brandId, String snCode) {
        return "KQACTIVATE:" + operatorId + ":" + brandId + ":" + snCode;
    }

    public static String getLsTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "LSTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String gethkTransLast(Integer operatorId, Integer brandId) {
        return "LASTSUCCESSTIME:HKTRANS:" + operatorId + ":" + brandId;
    }

    public static String gethkActivteLast(Integer operatorId, Integer brandId) {
        return "LASTSUCCESSTIME:HKACTIVTE:" + operatorId + ":" + brandId;
    }

    public static String getImplementInUsered(Integer operatorId, Integer brandId, String snCode, Integer awardModeId) {
        return "IMPLEMENTINUSERED:" + operatorId + ":" + brandId + ":" + snCode + ":" + awardModeId;
    }
    public static String getVipImplementInUsered(Integer operatorId, Integer brandId, String snCode, Integer awardModeId) {
        return "VIPIMPLEMENTINUSERED:" + operatorId + ":" + brandId + ":" + snCode + ":" + awardModeId;
    }
    public static String getImplementInUseredAndAgentIdAndModeId(Integer operatorId, Integer brandId,Integer modeId, Integer agentId, Integer awardModeId) {
        return "IMPLEMENTINUSERED:" + operatorId + ":" + brandId + ":"+ modeId + ":" + agentId + ":" + awardModeId;
    }


    public static String getImplementInUsered(Integer operatorId, Integer brandId, String snCode, Integer awardModeId, String yyyyMM) {
        return "IMPLEMENTINUSERED:" + operatorId + ":" + brandId + ":" + snCode + ":" + awardModeId + ":" + yyyyMM;
    }

    /**
     * 代理商品牌渠道交易笔数
     */
    public static String getPayAgentBrandChannelCount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_COUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getPayAgentBrandChannelCount(String date, Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_COUNT, date, operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getPayAgentBrandChannelCount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_COUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, "*");
    }

    /**
     * 代理商品牌渠道交易金额
     */
    public static String getPayAgentBrandChannelSumAmount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getPayAgentBrandChannelSumAmount(String date, Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT, date, operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getPayAgentBrandSumAmount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, "*");
    }

    /**
     * 匹配指定品牌下所有渠道数据
     */
    public static String getPayAgentBrandChannelSumAmount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, CommonConstant.SPLIT_ASTERISK);
    }

    /**
     * 匹配团队非直属交易数据
     */
    public static String getPayAgentBrandChannelSumAmount(Integer operatorId, String agentLevel) {
        return String.format(PAY_AGENT_BRAND_CHANNEL_SUM_AMOUNT_INDIRECT, getFormatDate(), operatorId, agentLevel) + CommonConstant.SPLIT_ASTERISK;
    }

    /**
     * 代理商品牌渠道返佣笔数
     */
    public static String getRebateAgentBrandChannelCount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(REBATE_AGENT_BRAND_CHANNEL_COUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getRebateAgentBrandChannelCount(String date, Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(REBATE_AGENT_BRAND_CHANNEL_COUNT, date, operatorId, agentLevel, agentId, brandId, channelId);
    }

    /**
     * 代理商品牌渠道返佣金额
     */
    public static String getRebateAgentBrandChannelSumAmount(Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(REBATE_AGENT_BRAND_CHANNEL_SUM_AMOUNT, getFormatDate(), operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getRebateAgentBrandChannelSumAmount(String date, Integer operatorId, String agentLevel, Integer agentId, Integer brandId, Integer channelId) {
        return String.format(REBATE_AGENT_BRAND_CHANNEL_SUM_AMOUNT, date, operatorId, agentLevel, agentId, brandId, channelId);
    }

    public static String getLsMultiOrderKey(Integer operatorId, Integer brandId, String type, String orderId) {
        return "LSORDER:" + operatorId + ":" + brandId + ":" + type + ":" + orderId;
    }

    public static String getStatisticHomeKey(Integer agentId, String reqType, String brandIdList) {
        return STATISTIC_HOME + agentId + "_" + reqType + "_" + brandIdList;
    }

    public static String getYLFHeaders(String staffLoginMobile) {
        return "YLF_" + staffLoginMobile;
    }

    public static String getQBHeaders(String staffLoginMobile) {
        return "QB_" + staffLoginMobile;
    }

    public static String getZFDQHeaders(String staffLoginMobile) {
        return "ZFDQ_" + staffLoginMobile;
    }

    public static String getZFDPOSHeaders(String staffLoginMobile) {
        return "ZFDPOS_" + staffLoginMobile;
    }

    public static String getLeShuaHeaders(String staffLoginMobile) {
        return "LeShua" + staffLoginMobile;
    }

    public static String getLdYsHeaders(String staffLoginMobile) {
        return "LdYs" + staffLoginMobile;
    }

    public static String getFortuneBillHeaders(String staffLoginMobile) {
        return "FortuneBill" + staffLoginMobile;
    }

    public static String getAllinpayHeaders(String staffLoginMobile) {
        return "Allinpay" + staffLoginMobile;
    }

    public static String getWoshuaHeaders(String staffLoginMobile) {
        return "WOSHUA" + staffLoginMobile;
    }

    public static String getTYFHeaders(String staffLoginMobile) {
        return "TYF" + staffLoginMobile;
    }

    public static String getSandpayHeaders(String staffLoginMobile) {
        return "SANDPAY" + staffLoginMobile;
    }

    public static String getLeshuaKlbHeaders(String staffLoginMobile) {
        return "LESHUAKLB" + staffLoginMobile;
    }

    public static String getLeshuaXhHeaders(String staffLoginMobile) {
        return "LESHUAXH" + staffLoginMobile;
    }

    public static String getLeshuaXhdqHeaders(String staffLoginMobile) {
        return "LESHUAXHDQ" + staffLoginMobile;
    }

    public static String getQupayHeaders(String staffLoginMobile) {
        return "QUPAY" + staffLoginMobile;
    }

    public static String getLakalapayHeaders(String staffLoginMobile) {
        return "LAKALAPAY" + staffLoginMobile;
    }

    public static String getLechuangLakalaHeaders(String staffLoginMobile) {
        return "LECHUANGLAKALA" + staffLoginMobile;
    }

    public static String getFuhuipayHeaders(String staffLoginMobile) {
        return "FUHUIPAY" + staffLoginMobile;
    }

    public static String getAnlvtechHeaders(String staffLoginMobile) {
        return "Anlvtech" + staffLoginMobile;
    }

    public static String getKZBHeaders(String staffLoginMobile) {
        return "KZB" + staffLoginMobile;
    }

    public static String getKZBPROHeaders(String staffLoginMobile) {
        return "KZBPRO" + staffLoginMobile;
    }

    public static String getKZBPLUSHeaders(String staffLoginMobile) {
        return "KZBPLUS" + staffLoginMobile;
    }

    public static String getUnauthorized() {
        return "UNAUTHORIZED_UPLOAD" + dateFormat.format(new Date());
    }

    public static String getAgentShutKey4Jg(String agentCode, String business) {
        return "AGENT_SHUT_JG:" + agentCode + ":" + business;
    }

    public static String getExcelTransOrderKey(Integer operatorId, Integer brandId, String transOrder) {
        return "EXCEL_TRANS_ORDER:" + operatorId + ":" + brandId + ":" + transOrder;
    }

    public static String getQMJFDOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "QMJFDORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String pypGateOrder(Integer operatorId, Integer brandId, String gateIdSeq) {
        return "PYPORDER:" + operatorId + ":" + brandId + ":" + gateIdSeq;
    }

    public static String getSumAmount4LlgFixedRate(Integer operatorId, Integer brandId, String snCode) {
        return "SUM_AMOUNT_LLG_FIXED_RATE:" + operatorId + ":" + brandId + ":" + snCode;
    }

    /**
     * 根据运营商以及品牌获取费率
     *
     * @param operatorId
     * @param brandId
     * @return
     */
    public static String getFee(Integer operatorId, Integer brandId, String feeTemplateId) {
        return String.format(OPERATOR_BRAND_FEE, operatorId, brandId, feeTemplateId);
    }

    /**
     * 获取拉卡拉交易订单RedisKey
     *
     * @param orderId
     */
    public static String getLklTradeOrderKey(String orderId) {
        return "LKLORDER:TRADE:" + orderId;
    }

    /**
     * 获取拉卡拉费用订单RedisKey
     *
     * @param orderId
     */
    public static String getLklFeeOrderKey(String orderId) {
        return "LKLORDER:FEE:" + orderId;
    }

    /**
     * 获取易宝交易订单RedisKey
     *
     * @param orderId
     */
    public static String getYbTradeOrderKey(String orderId) {
        return "YBORDER:TRADE:" + orderId;
    }

    /**
     * 获取易宝激活订单RedisKey
     *
     * @param orderId
     */
    public static String getYbActiveOrderKey(String orderId) {
        return "YBORDER:ACTIVE:" + orderId;
    }
    /**
     * 获取易宝激活订单RedisKey
     *
     * @param snCode
     */
    public static String getYbNewMerchantOrderKey(String snCode) {
        return "YBORDER:NEWMERCHANT:" + snCode;
    }

    public static String getBeforeSumAmountBySnCodeKey(Integer operatorId, Integer brandId, Integer agentId, String snCode, String preTwoDayString) {
        return "IMPLEMENT_SUM_AMOUNT:" + operatorId + ":" + brandId + ":" + agentId + ":" + snCode + ":" + preTwoDayString;
    }

    /**
     * 交易额范围内奖励防重
     */
    public static String getAmountRangeAwardRepeatAvoid(Integer operatorId, Integer brandId, String snCode, String todayString) {
        return "AMOUNT_RANGE_AWARD_REPEAT_AVOID:" + operatorId + ":" + brandId + ":" + snCode + ":" + todayString;
    }

    public static String getAgentUserCount(String agent) {
        return "AgentUerCount:" + agent;
    }

    /**
     * 缴费升级特定层级特定等级奖励防重
     */
    public static String getUpgradeLevelGradeLimitAwardRepeatAvoid(Integer operatorId, Integer brandId, String transOrder) {
        return "UPGRADE_LEVEL_GRADE_LIMIT_AWARD_REPEAT_AVOID:" + operatorId + ":" + brandId + ":" + transOrder;
    }

    /**
     * 获取开赚宝订单key
     */
    public static String getKzbOrderKey(String orderId) {
        return "KZBORDER:" + orderId;
    }

    public static String getAllinpayMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ALLINPAY:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getAnlvtechMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ANLVTECH:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getKzbMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "KZB:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getKzbProMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "KZBPRO:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getKzbPlusMposOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "KZBPLUS:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getCjOrderKey(String orderId) {
        return "CJORDER:" + orderId;
    }

    public static String getCjXHCOrderKey(String orderId) {
        return "CJXHCORDER:" + orderId;
    }

    public static String getKqTransFileKey(String checkkey) {
        return "KAIQIAN:" + checkkey;
    }

    public static String getKqActivateFileKey(String checkkey) {
        return "KAIQIAN:" + checkkey;
    }

    public static String checkIdNumberKey(String agentId) {
        return "checkIdNumber:" + agentId;
    }

    public static String getPointPaymentPasswordKey(Integer operatorId, Integer agentId) {
        return "PPP:" + operatorId + ":" + agentId;
    }

    public static String getDytTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "DYTORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getAnposSettleKey(String orderId) {
        return "ANPOS_ORDER_SETTLE:" + orderId;
    }

    public static String getAnposTradeKey(String orderId) {
        return "ANPOS_ORDER_TRADE:" + orderId;
    }

    public static String getAnposSettleRepeatKey(String orderId) {
        return "ANPOS_ORDER_SETTLE_REPEAT:" + orderId;
    }

    public static String getAnposTradeRepeatKey(String orderId) {
        return "ANPOS_ORDER_TRADE_REPEAT:" + orderId;
    }

    public static String getAnposHTLSettleKey(String orderId) {
        return "ANPOS_HTL_ORDER_SETTLE:" + orderId;
    }

    public static String getAnposHTLTradeKey(String orderId) {
        return "ANPOS_HTL_ORDER_TRADE:" + orderId;
    }

    public static String getAnposHTLSettleRepeatKey(String orderId) {
        return "ANPOS_HTL_ORDER_SETTLE_REPEAT:" + orderId;
    }

    public static String getAnposHTLTradeRepeatKey(String orderId) {
        return "ANPOS_HTL_ORDER_TRADE_REPEAT:" + orderId;
    }

    public static String getAnposHTLActivateRepeatKey(String snCode) {
        return "ANPOS_HTL_ACTIVATE_REPEAT:" + snCode;
    }

    public static String getYstongHeaders(String staffLoginMobile) {
        return "YST" + staffLoginMobile;
    }

    public static String getLeshuaxhlmHeaders(String staffLoginMobile) {
        return "LSDQXHLM" + staffLoginMobile;
    }

    public static String getYxbOrderKey(String orderId) {
        return "YXBORDER:" + orderId;
    }

    public static String getHaiPosOrderKey(String orderId) {
        return "HAIPOS:" + orderId;
    }
    public static String getRuiXinOrderKey(String orderId) {
        return "RUIXINCOMMON:" + orderId;
    }
    public static String getHaiefuOrderKey(String orderId) {
        return "HAIEFU:" + orderId;
    }

    public static String getHKactiveKey(String orderId) {
        return "HKXYFACTIVE:" + orderId;
    }
    public static String getYxbdqOrderKey(String orderId) {
        return "YXBDQORDER:" + orderId;
    }

    /**
     * 银盛通交易流水号Key
     *
     * @return
     */
    public static String getYstOrder(Integer operatorId, Integer brandId, String transNum) {
        return "YST:" + operatorId + ":" + brandId + ":" + transNum;
    }

    public static String getLsdqOrder(Integer operatorId, Integer brandId, String transNum) {
        return "LSDQ:" + operatorId + ":" + brandId + ":" + transNum;
    }

    public static String getAnposXHLMSettleKey(String orderId) {
        return "ANPOS_XHLM_ORDER_SETTLE:" + orderId;
    }

    public static String getAnposXHLMTradeKey(String orderId) {
        return "ANPOS_XHLM_ORDER_TRADE:" + orderId;
    }

    public static String getAnposXHLMSettleRepeatKey(String orderId) {
        return "ANPOS_XHLM_ORDER_SETTLE_REPEAT:" + orderId;
    }

    public static String getAnposXHLMTradeRepeatKey(String orderId) {
        return "ANPOS_XHLM_ORDER_TRADE_REPEAT:" + orderId;
    }

    public static String getAnposXHLMActivateRepeatKey(String snCode) {
        return "ANPOS_XHLM_ACTIVATE_REPEAT:" + snCode;
    }

    public static String getHkxhposOrder(String orderId) {
        return "HKXH_DPOS:TRADE:" + orderId;
    }

    public static String getSdjfposOrder(String orderId) {
        return "SDJF_DPOS:TRADE:" + orderId;
    }

    public static String getHkdkzposOrder(String orderId) {
        return "HKDKZ_DPOS:TRADE:" + orderId;
    }

    public static String getHkdposOrder(String orderId) {
        return "HK_DPOS:TRADE:" + orderId;
    }

    public static String getHuifumposOrder(String orderId) {
        return "HUIFU_MPOS:TRADE:" + orderId;
    }

    public static String getLsdpOrder(Integer operatorId, Integer brandId, String transNum) {
        return "LSDP:" + operatorId + ":" + brandId + ":" + transNum;
    }
    public static String getLdysTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "LDYSTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }
    public static String getLdysActivate(Integer operatorId, Integer brandId, String snCode) {
        return "LDYSACTIVATE:" + operatorId + ":" + brandId + ":" + snCode;
    }
    public static String getXtkTransOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "XTKTRANSORDER:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }
    /**
     * 获取盛付通交易订单RedisKey
     *
     * @param orderId
     */
    public static String getSFTTradeOrderKey(String orderId) {
        return "SFTORDER:TRADE:" + orderId;
    }
    public static String getXbdqSrxOrder(Integer operatorId, Integer brandId, String transNum) {
        return "XBDQ_SRX:" + operatorId + ":" + brandId + ":" + transNum;
    }


    public static String getJxbOrderKey(String orderId) {
        return "JXB_STANDARD:" + orderId;
    }

    public static String getOrderRepeatSetKey(Integer operatorId, String brandCode) {
        return "ORDER_REPEAT:" + operatorId + ":" + brandCode;
    }

    public static String getOrderRepeatSetKey(Integer operatorId, String brandCode, String otherSign) {
        return "ORDER_REPEAT:" + operatorId + ":" + brandCode + ":" + otherSign;
    }
    //团队育新奖每月1个奖励规则1个品牌1个月只能奖励1次
    public static String getTeamInnovationSetKey(Integer awardModeId,Integer agentId,String yyyyMM) {
    	return "TEAMINNOVATION:" + awardModeId + ":" + agentId + ":"+yyyyMM;
    }
    //固定机具数给予上级奖励 一个代理商只能给上级贡献1次
    public static String getFixedEquipmentSetKey(Integer awardModeId,Integer agentId) {
    	return "FIXEDEQUIPMENT:" + awardModeId + ":" + agentId ;
    }

    //押金机具激活奖励上级上上级
    public static String getAwardIndirectPush(Integer awardModeId,Integer agentId) {
    	return "AWARDINDIRECTPUSH:" + awardModeId + ":" + agentId ;
    }

    // 大POS激活奖励上机具所属人
    public static String getAwardBigPos(Integer operatorId, Integer brandId, String snCode, Integer awardModeId) {
        return "AWARDBIGPOS:" + operatorId + ":" + brandId + ":" + snCode + ":" + awardModeId;
    }
    
  //团队育新奖每月1个奖励规则1个品牌1个月只能奖励1次
    public static String getTeamInnovationSelfSetKey(Integer awardModeId,Integer agentId,String yyyyMM) {
    	return "TEAMINNOVATIONSELF:" + awardModeId + ":" + agentId + ":"+yyyyMM;
    }
    
  //团队育新奖每月1个奖励规则1个品牌1个月只能奖励1次
    public static String getLiShuaTeamInnovationSetKey(Integer awardModeId,Integer agentId,String yyyyMM) {
    	return "LSTEAMINNOVATION:" + awardModeId + ":" + agentId + ":"+yyyyMM;
    }

    /**
     * 腾付通
     * @param orderId
     * @return
     */
    public static String getTftTradeOrderKey(String orderId) {
        return "TFTORDER:TRADE:" + orderId;
    }
    public static String getTftActiveOrderKey(String orderId) {
        return "TFTORDER:ACTIVE:" + orderId;
    }

    public static String getBenefitOrderKey(String orderId) {
        return "BENEFIT:" + orderId;
    }
    
    public static String getWpTranOrder(String orderId) {
        return "WP:TRAN:" + orderId;
    }
    
    public static String getWpActivaOrder(String orderId) {
        return "WP:ACTIVA:" + orderId;
    }
    
    public static String getHkActivaOrder(String orderId) {
        return "HK:ACTIVA:" + orderId;
    }
    
    public static String getHkTranOrder(String orderId) {
        return "HK:TRAN:" + orderId;
    }
    public static String getHkXefTranOrder(String orderId) {
        return "HKXEF:TRAN:" + orderId;
    }

    public static String getZfZmtOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ZFZMTTRANS:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getZfZmtActivate(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ZFZMTACTIVATE:" + operatorId + ":" + brandId + ":" + tradeSerial;
    }

    public static String getDyTranOrder(String orderId) {
        return "DY:TRAN:" + orderId;
    }
    public static String getXYFTradeOrderKey(String orderId) {
        return "XYFORDER:TRADE:" + orderId;
    }
    public static String getKdbTranOrder(String orderId) {
        return "KDB:TRAN:" + orderId;
    }
    public static String getKdbActivaOrder(String orderId) {
        return "KDB:ACTIVA:" + orderId;
    }
    public static String getDyActivaOrder(String orderId) {
        return "DY:ACTIVA:" + orderId;
    }
    public static String getSftZybTranOrder(String orderId) {
        return "SFTZYB:TRAN:" + orderId;
    }
    public static String getSftZybActivaOrder(String orderId) {
        return "SFTZYB:ACTIVA:" + orderId;
    }

    public static String getQbTranOrder(String orderId) {
        return "QB:TRAN:" + orderId;
    }
    
    public static String getZfTranOrder(Integer operatorId, Integer brandId, String tradeSerial) {
        return "ZF:TRAN:" + tradeSerial;
    }
    
    public static String getHkNposTranOrder(String orderId) {
        return "HKN:TRAN:" + orderId;
    }
    
    public static String getLsMOrderKey(Integer operatorId, Integer brandId, String type, String orderId) {
        return "LSM:TRAN:" + operatorId + ":" + brandId + ":" + type + ":" + orderId;
    }
    
    public static String getLsZxOrderKey(Integer operatorId, Integer brandId, String type, String orderId) {
        return "LSZXORDER:" + operatorId + ":" + brandId + ":" + type + ":" + orderId;
    }
}
