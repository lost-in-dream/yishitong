package org.yishitong.common.utils;

import java.math.BigDecimal;

public class CommonConstant {

	public static final String SUCCESS_CODE = "0000";
	public static final String PAY_SUCCESS_CODE = "000000";

	/**
	 * 状态值
	 * 0 禁止 1 正常
	 */
	public static final String STATUS_ZERO = "0";
	public static final String STATUS_ONE = "1";

	/**
	 * 是否  0否  1是
	 */
	public static final String STATUS_NO = "0";
	public static final String STATUS_YES = "1";
	public static final String STATUS_PART = "2";

	/**
	 * 账户类型 1 返佣
	 */
	public static final String ACCOUNT_TYPE_REBATE = "1";
	public static final String ACCOUNT_TYPE_POINT = "2";

	/**
	 * 账户状态 0 禁止 1 正常 2 只进不出 3 只出不进
	 */
	public static final String ACCOUNT_STATUS_STOP = "0";
	public static final String ACCOUNT_STATUS_NORMAL = "1";
	public static final String ACCOUNT_STATUS_TWO = "2";
	public static final String ACCOUNT_STATUS_THREE = "3";

	/**
	 * 24小时内，最大登录错误次数
	 */
	public static final int MAX_LOGIN_ERROR = 5;

	/**
	 * 手机号长度
	 */
	public static final int MOBILE_LENGTH = 11;

	/**
	 * token长度
	 */
	public static final int TOKEN_LENGTH = 32;

	/**
	 * 操作模块
	 */
	public static final String ACTION_MODULE_LOG = "1";
	public static final String ACTION_MODULE_AGENTLIST = "2";
	/**
	 * 操作类型 （针对某个操作模块）
	 */
	public static final String ACTION_TYPE_LOG_IN = "1";
	public static final String ACTION_TYPE_LOG_OUT = "2";

	/**
	 * 操作结果
	 */
	public static final String ACTION_RESULT_FAIL = "0";
	public static final String ACTION_RESULT_SUCC = "1";

	/**
	 * 请求头
	 */
	public static final String HEADER_OPERATOR_ID = "operatorId";
	public static final String HEADER_APP_ID = "appId";
	public static final String HEADER_TOKEN_ID = "tokenId";
	public static final String ENCRY_PT_RSA = "encryptrsa";
	public static final String DES_KEY = "deskey";
	public static final String HEADER_REALLY_URL = "headreallyurl";

	/**
	 * 请求追踪id
	 */
	public static final String ZUUL_TRACK_ID = "trackId";

	/**
	 * 是否认证token
	 */
	public static final String HEADER_AUTH_TOKEN = "authToken";

	/**
	 * 默认头像地址
	 */
	//public static final String DEFAULT_HEAD_IMG = "https://pro-3-0.oss-cn-hangzhou.aliyuncs.com/proadmin/tx%403x.png";
	public static final String DEFAULT_HEAD_IMG = "https://pro-service.oss-cn-zhangjiakou.aliyuncs.com/proadmin/tx%403x.png";

	/**
	 * 开通代理默认密码 （app666dev666）
	 */
	public static final String DEFAULT_PASS = "BC943AF691F3D32BFC14EB48A9B2E679";
	public static final String DEFAULT_PASS_WITHOUT_SLOT = "B6FC804F30BAA3170165AF0F3CD5FEB1";

	/**
	 * 顶级代理默认层级
	 */
	public static final String DEFAULT_AGENT_LEVEL = "0,";

	/**
	 * 顶级用户默认层级
	 */
	public static final String DEFAULT_USER_LEVEL = "0,";

	/**
	 * 分隔符
	 */
	public static final String SPLIT_COMMA = ",";
	public static final String SPLIT_SEMICOLON = ";";
	public static final String SPLIT_VERTICAL = "~";
	public static final String SPLIT_COLON = ":";
	public static final String SPLIT_ASTERISK = "*";
	public static final String SPLIT_COMMA_FULL = "，";
	public static final String SPLIT_UNDERLINE = "_";
	public static final String SPLIT_POINT = ".";
	public static final String SPLIT_TWO_POINT = "..";
	public static final String SPLIT_QUOTATION = "\"";

	/**
	 * 返佣状态(0未返佣，1已返佣)
	 */
	public static final Integer REBATE_STATUS_YES = 1;
	public static final Integer REBATE_STATUS_NO = 0;

	/**
	 * 冻结状态(0未冻结，1冻结)
	 */
	public static final Integer FORZEN_YES = 1;
	public static final Integer FORZEN_NO = 0;

	/**
	 * 是否已插入待返佣表 0未计算返佣 1已插入待返佣表 2计算后无返佣数据
	 */
	public static final String ALREADY_REBATE_NO = "0";
	public static final String ALREADY_REBATE_YES = "1";
	public static final String ALREADY_REBATE_YES_NULL = "2";

	/**
	 * 分润模式类型 1费率差模式 2按考核分润
	 */
	public static final String MODE_TYPE_ONE = "1";
	public static final String MODE_TYPE_TWO = "2";

	/**
	 * 返佣层级 1 上级 2 上上级 3 上上上级
	 * 4 上四级 5 上五级  6上六级
	 * 0 代理 (2020年9月22日14:23:57 将代理从用4表示替换成用0表示，防止和返佣层级重复导致返佣异常。)
	 */
	public static final Integer REBATE_LEVEL_U = 1;
	public static final Integer REBATE_LEVEL_UU = 2;
	public static final Integer REBATE_LEVEL_UUU = 3;
	public static final Integer REBATE_LEVEL_4U = 4;
	public static final Integer REBATE_LEVEL_5U = 5;
	public static final Integer REBATE_LEVEL_6U = 6;
	public static final Integer REBATE_LEVEL_AGENT = 0;

	/**
	 * 代理商来源 数据来源 1 注册 2 导入 3 开通
	 */
	public static final String AGENT_SOURCE_REG = "1";
	public static final String AGENT_SOURCE_IMP = "2";
	public static final String AGENT_SOURCE_OPEN = "3";

	/**
	 * 描述内容最大长度限制
	 */
	public static final Integer DESC_MAX_LENGTH = 50;

	/**
	 * 表示无效
	 */
	public static final Integer UNUSED_VALUE = -1;

	/**
	 * 限制下载数量
	 */
	public static final Integer DOWNLOAD_MAX_NUM = 100000;

	/**
	 * 负一
	 */
	public static final Integer MINUS_ONE = -1;

	/**
	 * 零
	 */
	public static final Integer ZERO = 0;

	/**
	 * 壹
	 */
	public static final Integer ONE = 1;

	/**
	 * 贰
	 */
	public static final Integer TWO = 2;

	/**
	 * 叁
	 */
	public static final Integer THREE = 3;

	/**
	 * 零
	 */
	public static final String ZERO_STRING = "0";

	/**
	 * 壹
	 */
	public static final String ONE_STRING = "1";

	/**
	 * 贰
	 */
	public static final String TWO_STRING = "2";

	/**
	 * 叁
	 */
	public static final String THREE_STRING = "3";

	/**
	 * 用户来源 注册:RG，开通:OP，导入:IP，订单:SO，推广：PR ,其他:OT,app开通：AP
	 */
	public static final String USER_SOURCE_RG = "RG";
	public static final String USER_SOURCE_OP = "OP";
	public static final String USER_SOURCE_IP = "IP";
	public static final String USER_SOURCE_SO = "SO";
	public static final String USER_SOURCE_OT = "OT";
	public static final String USER_SOURCE_PR = "PR";
	public static final String USER_SOURCE_AP = "AP";
	/**
	 * YES OR NO
	 */
	public static final String YES = "Y";
	public static final String NO = "N";

	/**
	 * 默认佣金比例
	 */
	public static final BigDecimal REBATE_SCALE_DEFAULT = BigDecimal.valueOf(1);

	/**
	 * 默认登录密码
	 */
	public static String STAFF_PASSWORD_DEFAULT = "111111";


	/**
	 * 短信平台编码相关
	 */
	public static String SMS_CODE_CL = "CL";
	public static String SMS_CODE_AL = "AL";

	/**
	 * 三要素验证状态
	 */
	public static String REAL_POST_STATUS_NOT = "0";
	public static String REAL_POST_STATUS_SUCCESS = "1";
	public static String REAL_POST_STATUS_FAILED = "2";

	/**
	 * 验证码参数名称
	 */
	public static String G = "g";

	/**
	 * 后台验证码有效期 分钟
	 */
	public static final Integer ADMIN_SMS_CODE_EXPIRE = 10;
	/**
	 * 后台验证码发送间隔
	 */
	public static final Integer ADMIN_SMS_CODE_FIX = 2;
	/**
	 * 后台验证码长度
	 */
	public static final Integer ADMIN_SMS_CODE_LENGTH = 6;


	/**
	 * 后台验证码类型-开通代理商
	 */
	public static String ADMIN_SMS_OPEN_AGENT = "open_agent";

	/**
	 * 后台验证码类型-登陆验证
	 */
	public static String ADMIN_SMS_LOGIN = "sms_login";

	/**
	 * 卡信息三要素校验每日限制次数
	 */
	public static final Integer CARD_THREE_AUTH = 3;


	/**
	 * 后台验证码类型-短信登陆
	 */
	public static String USER_SMS_LOGIN = "login";
	public static String SMS_VERIFYCODE = "verifyCode";

	/**
	 * 卡bin长度
	 */
	public static final Integer BIN_CODE_LENGTH = 8;

	/**
	 * 账户流水表trade_type 1 返佣  2 提现 3解冻 4调账 5奖励 6积分自动兑 7积分手动兑
	 */
	public static final String TRADE_TYPE_REBATE = "1";
	public static final String TRADE_TYPE_WITHDRAW = "2";
	public static final String TRADE_TYPE_UNFREEZE = "3";
	public static final String TRADE_TYPE_ADJUSTMENT = "4";
	public static final String TRADE_TYPE_AWARD = "5";
	public static final String TRADE_TYPE_POINT_AUTO = "6";
	public static final String TRADE_TYPE_POINT_HAND = "7";
	public static final String TRADE_TYPE_IMPLEMENT_BUY = "8";
	public static final String TRADE_TYPE_BENEFIT = "9";
	public static final String TRADE_TYPE_FAKE_ACTIVATE = "A";

	/**
	 * 账户流水表trade_mark + 入账 - 出账 o 解冻 f 手动冻结 u 手动解冻
	 */
	public static final String TRADE_MARK_IN = "+";
	public static final String TRADE_MARK_OUT = "-";
	public static final String TRADE_MARK_UNFREEZE = "o";
	public static final String TRADE_MARK_HANDFREEZE = "f";
	public static final String TRADE_MARK_HANDUNFREEZE = "u";

	/**
	 * 账户流水表bus_type A提现 B提现退款 U自动解冻 T调账 AW 奖励 AJ 积分自动兑 HJ积分手动兑  ZM掌门活动
	 */
	public static final String BUS_TYPE_WITHDRAW = "A";
	public static final String BUS_TYPE_WITHDRAW_REFUND = "B";
	public static final String BUS_TYPE_AUTO_UNFREEZE = "U";
	public static final String BUS_TYPE_ADJUSTMENT = "T";
	public static final String BUS_TYPE_AWARD = "AW";
	public static final String BUS_TYPE_POINT_AUTO = "AJ";
	public static final String BUS_TYPE_POINT_HAND = "HJ";
	public static final String BUS_TYPE_IMPLEMENT_BUY = "IB";
	public static final String BUS_TYPE_IMPLEMENT_REFUND = "IR";
	public static final String BUS_TYPE_BENEFIT_AWARD = "BA";
	public static final String BUS_TYPE_FAKE_ACTIVATE = "FA";
	public static final String BUS_TYPE_ZM_ACTIVITY = "ZM";

	/**
	 * 积分流水表业务类型
	 * 0 历史数据默认值
	 * 1 交易
	 * 2 激活
	 * 3 调账
	 * 4 转账
	 * 5 积分兑换
	 * 6 购买
	 * 7 积分置换
	 * 8 升级
	 * 9排点兑换积分
	 */
	public static final String P_TRADE_BUS_TYPE_HISTORY = "0";
	public static final String P_TRADE_BUS_TYPE_TRADE = "1";
	public static final String P_TRADE_BUS_TYPE_ACTIVATE = "2";
	public static final String P_TRADE_BUS_TYPE_ADJUSTMENT = "3";
	public static final String P_TRADE_BUS_TYPE_TRANSFER = "4";
	public static final String P_TRADE_BUS_TYPE_EXCHANGE = "5";
	public static final String P_TRADE_BUS_TYPE_BUY = "6";
	public static final String P_TRADE_BUS_TYPE_PERM = "7";
	public static final String P_TRADE_BUS_TYPE_UPGRADE = "8";
	public static final String P_TRADE_BUS_TYPE_SPOT = "9";

	/**
	 * 积分流水表详细业务类型
	 * 积分返佣层级 1/2/3等等，现有积分
	 * A 机具交易赠送积分
	 * B 激活赠送积分
	 * C 调账调入
	 * D 调账调出
	 * E 积分转账转入
	 * F 积分转出
	 * G 手动兑换
	 * H 自动兑换
	 * I 积分购买
	 * J 积分购买退款
	 * K 积分置换
	 * L 购买机具送积分
	 * M 购买商品送积分
	 * N 升级送积分
	 * O 碰一碰交易送积分
	 * P 新功能商城扣除积分
	 * S 排点兑换积分
	 * XT 每日分享赠送积分
	 */
	public static final String P_BUS_TYPE_IMPLEMENT_TRADE_GIVE = "A";
	public static final String P_BUS_TYPE_ACTIVATE_GIVE = "B";
	public static final String P_BUS_TYPE_ADJUSTMENT_IN = "C";
	public static final String P_BUS_TYPE_ADJUSTMENT_OUT = "D";
	public static final String P_BUS_TYPE_TRANSFER_IN = "E";
	public static final String P_BUS_TYPE_TRANSFER_OUT = "F";
	public static final String P_BUS_TYPE_EXCHANGE_HAND = "G";
	public static final String P_BUS_TYPE_EXCHANGE_AUTO = "H";
	public static final String P_BUS_TYPE_BUY = "I";
	public static final String P_BUS_TYPE_BUY_REFUND = "J";
	public static final String P_BUS_TYPE_PERM = "K";
	public static final String P_BUS_TYPE_BUY_IMPLEMENT_GIVE = "L";
	public static final String P_BUS_TYPE_BUY_OTHER_GIVE = "M";
	public static final String P_BUS_TYPE_UPGRADE_GIVE = "N";
	public static final String P_BUS_TYPE_HIT_TRADE_GIVE = "O";
	public static final String P_BUS_TYPE_SPOT_PONIT = "S";
	public static final String P_BUS_TYPE_GOODS_PONIT = "P";
	public static final String P_BUS_TYPE_GOODS_PONIT_BACK = "Q";
	public static final String P_BUS_TYPE_SCHOOL_SHARE = "XT";

	/**
	 * 定时任务条数限制
	 */
	public static final Integer JOB_LIMIT = 1000;

	/**
	 * 最大长度限制 20
	 */
	public static final Integer MAX_LENGTH_TWENTY = 20;

	/**
	 * 100
	 */
	public static final Integer ONE_HUNDRED = 100;

	/**
	 * 极光状态修改时间控制
	 */
	public static final Integer TRIGGER_JG_TIME = 5;

	/**
	 * 提醒设置类型
	 */
	public static final String SHUT_TYPE_SMS = "1";
	public static final String SHUT_TYPE_JG = "2";

	/**
	 * 订单号长度
	 */
	public static final Integer ORD_LENGTH = 20;

	/**
	 * 订单状态-支付系统
	 */
	public static final String ORD_STAT_S = "S";
	public static final String ORD_STAT_F = "F";
	public static final String ORD_STAT_I = "I";

	/**
	 * 支付系统账户类型 R 充值  M 短信 A 认证
	 */
	public static final String PAY_ACCOUNT_R = "R";
	public static final String PAY_ACCOUNT_M = "M";
	public static final String PAY_ACCOUNT_A = "A";

	/**
	 * 订单状态：0交易初始，1交易成功，2交易失败，3交易中
	 */
	public static final Integer ORD_STAT_INIT = 0;
	public static final Integer ORD_STAT_SUCC = 1;
	public static final Integer ORD_STAT_FAIL = 2;
	public static final Integer ORD_STAT_ING = 3;

	/**
	 * 6
	 */
	public static final Integer SIX = 6;

	/**
	 * 金额统计类型 0 当日 1 当月
	 */
	public static final Byte COUNT_AMOUNT_TYPE_DAY = 0;
	public static final Byte COUNT_AMOUNT_TYPE_MONTH = 1;

	/**
	 * 统一回调方法名称
	 */
	public static final String CALLBACK = "CALLBACK";

	/**
	 * 返佣结算模式 （周结1，日结2，月结3）
	 */
	public static final String REBATE_PAY_TYPE_WEEK = "1";
	public static final String REBATE_PAY_TYPE_DAY = "2";
	public static final String REBATE_PAY_TYPE_MONTH = "3";

	/**
	 * 三十
	 */
	public static final Integer THIRTY = 30;

	/**
	 * APP 服务时间最大长度
	 */
	public static final Integer MAX_APP_CUS_TIME = 20;

	/**
	 * APP 联系地址最大长度
	 */
	public static final Integer MAX_APP_CONTACT = 200;

	/**
	 * 导入配置
	 * 0 代理商 1 用户
	 */
	public static final String IMPORT_AGENT = "0";
	public static final String IMPORT_USER = "1";

	/**
	 * 卡种 1 借记卡 2 贷记卡 3 预付费卡 4 准贷记卡
	 */
	public static final String CARD_TYPE_DEBIT = "1";
	public static final String CARD_TYPE_CREDIT = "2";
	public static final String CARD_TYPE_PREPAID = "3";
	public static final String CARD_TYPE_QUASI_CREDIT = "4";

	/**
	 * 机具类型implement_type
	 */
	public static final String IMPLEMENT_TYPE_POS = "POS";
	public static final String IMPLEMENT_TYPE_MPOS = "MPOS";

	/**
	 * 机具库存类型
	 */
	public static final String IMPLEMENT_STOCK_TYPE_IN = "1";
	public static final String IMPLEMENT_STOCK_TYPE_OUT = "2";
	public static final String IMPLEMENT_STOCK_TYPE_RECYCL = "3";

	/**
	 * 机具信息/记录状态
	 */
	public static final String IMPLEMENT_STATUS_IN = "1";
	public static final String IMPLEMENT_STATUS_OUT = "2";
	public static final String IMPLEMENT_STATUS_USED = "3";

	public static final String TERMINAL_STATUS_ACTIVATED = "已激活";
	public static final String TERMINAL_STATUS_NOACTIVATE = "未激活";
	public static final String TERMINAL_STATUS_TRADED = "有交易";
	public static final String TERMINAL_STATUS_NOTRADE = "无交易";

	/**
	 * 机具订单状态 (0待付款1待发货2待收货3已完成4已删除5支付中)
	 */
	public static final String IMPLEMENT_ORDER_WAIT_PAY = "0";
	public static final String IMPLEMENT_ORDER_WAIT_SEND = "1";
	public static final String IMPLEMENT_ORDER_WAIT_RECEIVING = "2";
	public static final String IMPLEMENT_ORDER_END = "3";
	public static final String IMPLEMENT_ORDER_DELETE = "4";
	public static final String IMPLEMENT_ORDER_PAYING = "5";

	/**
	 * 机具订单支付状态 (0初始1成功2失败)
	 */
	public static final String IMPLEMENT_PAY_WAIT_WAIT = "0";
	public static final String IMPLEMENT_PAY_WAIT_SUSS = "1";
	public static final String IMPLEMENT_PAY_WAIT_FAIL = "2";

	/**
	 * 机具购买类型1线上 2线下
	 */
	public static final String IMPLEMENT_BUY_ONLINE = "1";
	public static final String IMPLEMENT_BUY_OFFLINE = "2";

	/**
	 * 账户流水说明文案
	 */
	public static final String ACCOUNT_LOG_REMARK_WITHDRAW = "提现";
	public static final String ACCOUNT_LOG_REMARK_WITHDRAW_REFUND = "提现退款";

	/**
	 * 订单超时时间设置
	 */
	public static final Integer ORDER_TIMEOUT_PAY = -30;
	public static final Integer ORDER_TIMEOUT_RECEIVE = -30;

	/**
	 * 请求支付系统的账户类型
	 */
	public static final String PAY_ACCOUNT_TYPE_CASH = "CASH";
	public static final String PAY_ACCOUNT_TYPE_ROR0 = "NOR0";

	/**
	 * sql语句中in参数最大数量
	 */
	public static final Integer IN_MAX = 10000;

	public static final String AGENT_TRANSFER_TYPE_UPAGENT = "1";


	public static final String ADJUSTMENT_STATUS_WAIT = "1";
	public static final String ADJUSTMENT_STATUS_PASS = "2";
	public static final String ADJUSTMENT_STATUS_REJECT = "3";

	/**
	 * OrderPay 网关支付 H5
	 * CreateOrder 订单登记
	 * CreateChnlMer 通道商户开通
	 * pypTagEffect 碰一碰生效
	 * EposPay 无卡消费
	 * EposSmsSubmit 无卡消费确认
	 * CashPay 代付
	 */
	public static final String REQTYPE_ORDERPAY = "OrderPay";
	public static final String REQTYPE_CREATEORDER = "CreateOrder";
	public static final String REQTYPE_CREATECHNLMER = "CreateChnlMer";
	public static final String REQTYPE_PYPTAGEFFECT = "pypTagEffect";
	public static final String REQTYPE_EPOSPAY = "EposPay";
	public static final String REQTYPE_EPOSSMSSUBMIT = "EposSmsSubmit";
	public static final String REQTYPE_CASHPAY = "CashPay";

	/**
	 * X 信用卡
	 * J 借记卡
	 */
	public static final String CARD_TYPE_XYK = "X";
	public static final String CARD_TYPE_CXK = "J";

	public static final String PUSH_TYPE_ACTIVE = "A";
	public static final String PUSH_TYPE_VIP = "V";
	public static final String PUSH_TYPE_TRADE = "T";

	public static final String BRAND_BUS_TYPE_HT = "HT";
	public static final String BRAND_BUS_TYPE_XX = "XX";
	public static final String BRAND_BUS_TYPE_XS = "XS";
	public static final String BRAND_BUS_TYPE_ZZ = "ZZ";

	public static final String BRAND_BUS_TYPE_DESC_HT = "[碰一碰类]";
	public static final String BRAND_BUS_TYPE_DESC_XS = "[线上收单类]";
	public static final String BRAND_BUS_TYPE_DESC_XX = "[线下收单类]";
	public static final String BRAND_BUS_TYPE_DESC_ZZ = "[增值类]";

	public static final String ACTION_TYPE_LOGIN = "1";
	public static final String ACTION_TYPE_LOGOUT = "2";
	public static final String ACTION_TYPE_CHANGEPARENT = "3";
	public static final String ACTION_TYPE_CHANGEAGENT = "4";
	/**
	 * 分润模式
	 * RATE_DIFFER：费率差
	 * TEAM_MANAGE：费率差+团队管理奖
	 * DIF_FIXED_RATE：终端费率差+固定费率
	 * FIXED_RATE：固定费率
	 * LLG_FIXED_RATE：限级固定费率
	 */
	public static final String REBATE_MODE_AMT_DIFFER = "AMT_DIFFER";
	public static final String REBATE_MODE_RATE_DIFFER = "RATE_DIFFER";
	public static final String REBATE_MODE_TEAM_MANAGE = "TEAM_MANAGE";
	public static final String REBATE_MODE_DIF_FIXED_RATE = "DIF_FIXED_RATE";
	public static final String REBATE_MODE_FIXED_RATE = "FIXED_RATE";
	public static final String REBATE_MODE_LLG_FIXED_RATE = "LLG_FIXED_RATE";
	public static final String REBATE_MODE_TRANS_LIQ_DIF = "TRANS_LIQ_DIF";
	public static final String REBATE_MODE_RATE_DIF_FIXED = "RATE_DIF_FIXED";

	public static final String AUTO_UPGRADE_OPERATOR = "AUTO_OPERATOR";

	public static final String LOGICAL_OPERATOR_AND = "1";
	public static final String LOGICAL_OPERATOR_OR = "2";

	/**
	 * 激活类型 1 手动激活 2 真实激活
	 */
	public static final String ACTIVITY_MANUAL = "1";
	public static final String ACTIVITY_REAL = "2";
	public static final String ACTIVITY_IMPORT = "3";
	public static final String ACTIVITY_IMPORT_DESC = "IMPORT_ACTIVATION";
	public static final int DAY_UNAUTHORIZED_UPLOAD_MAX = 1000;

	/**
	 * 商户进件状态 3 未提交过 2 审核中 0 认证失败 1 认证成功
	 */
	public static final String MERCHANT_ENTRY_STATUS_INIT = "3";
	public static final String MERCHANT_ENTRY_STATUS_ING = "2";
	public static final String MERCHANT_ENTRY_STATUS_FAILED = "0";
	public static final String MERCHANT_ENTRY_STATUS_SUCCESS = "1";

	/**
	 * null字符串
	 */
	public static final String NULL_STRING = "null";

	/**
	 * 商品设置中最多上传图片个数
	 */
	public static final int IMAGES_MAX_SIZE = 5;

	/**
	 * 积分商品订单状态 0 待发货 1 待收货 2 已完成
	 */
	public static final String POINT_COMMODITY_ORDER_STATUS_SENDING = "0";
	public static final String POINT_COMMODITY_ORDER_STATUS_RECEIVING = "1";
	public static final String POINT_COMMODITY_ORDER_STATUS_SUCCESS = "2";

	/**
	 * 撞击者tokenId前缀
	 */
	public static final String HIT_TOKEN_PREFIX = "HIT_";

	/**
	 * 碰一碰登录token长度
	 */
	public static final int TOKEN_LENGTH_HIT = 36;

	/**
	 * 碰一碰登录，如果需要选择运营商，返回该tokenId值
	 */
	public static final String TOKEN_ID_CHOOSE_OPERATOR = "CHOOSE_OPERATOR";

	/**
	 * t_implement_user_record表类型 1 绑定 2 解绑 3 激活
	 */
	public static final String IMPLEMENT_USER_RECORD_TYPE_BIND = "1";
	public static final String IMPLEMENT_USER_RECORD_TYPE_UNTIE = "2";
	public static final String IMPLEMENT_USER_RECORD_TYPE_ACTIVE = "3";

	/**
	 * 碰一碰激活缴费状态 0 预支付 1 确认支付 2 支付成功 3 支付失败
	 */
	public static final String HIT_ACTIVE_PAY_STATUS_PREPAY = "0";
	public static final String HIT_ACTIVE_PAY_STATUS_CONFIRM = "1";
	public static final String HIT_ACTIVE_PAY_STATUS_SUCCESS = "2";
	public static final String HIT_ACTIVE_PAY_STATUS_FAIL = "3";

	/**
	 * 碰一碰代付状态
	 */
	public static final String HIT_WITHDRAW_STAT_I = "0";
	public static final String HIT_WITHDRAW_STAT_S = "1";
	public static final String HIT_WITHDRAW_STAT_F = "2";

	/**
	 * 3.0自动生成单号长度
	 */
	public static final int ORDER_LENGTH = 20;

	/**
	 * 碰一碰交易订单交易状态
	 * 1 成功
	 */
	public static final int HIT_TRANS_STATUS_S = 1;

	/**
	 * 碰一碰重新代付计算加法位数
	 */
	public static final int HIT_REWITHDRAW_ADD_BIT = 5;

	/**
	 * 碰一碰默认型号（所有品牌通用）
	 */
	public static final String MODEL_HIT_DEFAULT = "DEFAULT";

	/**
	 * 交易送积分订单号前缀
	 */
	public static final String PAY_POINT_PREFIX_TRANSACTION = "JY";
	public static final String PAY_POINT_PREFIX_HIT_TRANSACTION = "HITJY";


	/**
	 * 支付系统网关定义
	 */
	public static final String GATE_HLB_PYP = "hlbpyp";
	public static final String GATE_HLB_PYP_DE = "hlbpypde";
	public static final String GATE_DY_PYP = "dypyp";
	public static final String GATE_DY_PYP_DE = "dypypde";
	public static final String GATE_DY_PYP_DF = "dfdypyp";
	/**
	 * 快钱
	 */
	public static final String KQ_ALL_DATA = "ALL";

	/**
	 * 积分支付密码错误次数限制
	 */
	public static final int POINT_PAYMENT_PASSWORD_OVERTIMES = 3;

	/**
	 * 鼓励后缀
	 */
	public static final String ENCOURAGE_SUFFIX_CHANNEL_CODE = "_E";
	public static final String ENCOURAGE_SUFFIX_CHANNEL_NAME = "（鼓励期）";

	/**
	 * 人工审核激活审核状态
	 */

	public static final String IMPLEMENT_CHECK_STATUS_INIT = "0";
	public static final String IMPLEMENT_CHECK_STATUS_PASS = "1";
	public static final String IMPLEMENT_CHECK_STATUS_REJECT = "2";
	public static final String IMPLEMENT_CHECK_STATUS_NO = "3";

	/**
	 * 积分置换机具状态  0 初始,1 失败,2 成功
	 */
	public static final String PERMUTATION_STAT_INIT = "0";
	public static final String PERMUTATION_STAT_FAIL = "1";
	public static final String PERMUTATION_STAT_SUCCESS = "2";

	/**
	 * 积分转账类型
	 */
	public static final String POINT_TRANSFER_TRANSFER = "1";
	public static final String POINT_TRANSFER_PERM = "2";

	/**
	 * 交易数据来源
	 * 0 技术手段导入
	 * 1 excel表格导入
	 */
	public static final String TRANS_SOURCE_BY_TECHNOLOGY = "0";
	public static final String TRANS_SOURCE_BY_EXCEL = "1";

	public static final String MER_PRIV_EXCEL_MARK = "FOR_EXCEL";

	public static final int HIT_SERVER_HOST_MIN_LENGTH = 6;

	/**
	 * 0 团队 1 直营 2 非直营
	 */
	public static final String DATA_TEAM = "0";
	public static final String DATA_DIRECT = "1";
	public static final String DATA_INDIRECT = "2";

	/**
	 * 为兼容老版本，需要定义一个老版本标识
	 */
	public static final String FOR_OLD_VERSION = "OLD";
}
