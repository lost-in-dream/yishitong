package org.yishitong.common.utils;


import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;


/**
 * 有关字符串处理的工具类。
 *
 * @author PanLiTao
 */
public class StringUtil extends StringUtils {

	private static final String CHARSET_NAME = "UTF-8";
	public static final String EMPTY_L = "EMPTY";

	public static Pattern pattern = Pattern.compile("[0-9]*");

	/**
	 * 首字母小写
	 */
	public static String lowerFirst(String str) {
		if (isBlank(str)) {
			return "";
		} else {
			return str.substring(0, 1).toLowerCase() + str.substring(1);
		}
	}

	/**
	 * 首字母大写
	 */
	public static String upperFirst(String str) {
		if (isBlank(str)) {
			return "";
		} else {
			return str.substring(0, 1).toUpperCase() + str.substring(1);
		}
	}

	/**
	 * 判断字符是否是数字
	 *
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		return pattern.matcher(str).matches();
	}

	/**
	 * 自动升级构造数据专用
	 *
	 * @param operatorId
	 * @param brandId
	 * @param gradeId
	 * @return
	 */
	public static String genAutoKey(Integer operatorId, Integer brandId, Integer gradeId) {
		String gradeString = gradeId == null ? EMPTY_L : gradeId.toString();
		return operatorId.toString() + CommonConstant.SPLIT_VERTICAL + brandId + CommonConstant.SPLIT_VERTICAL + gradeString;
	}

}