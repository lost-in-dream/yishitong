package org.yishitong.common.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;

public class DesUtils {
    private static String DesKeySpec = "DESede";
    // 加解密统一使用的编码方式
    private final static String encoding = "utf-8";

    /**
     * 转换成十六进制24位字符串
     */
    public static byte[] hex(String key) {
        String f = DigestUtils.md5Hex(key).toLowerCase();
        try {
            return f.substring(0, 24).getBytes(encoding);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 3DES加密
     *
     * @param key    密钥，24位
     * @param srcStr 将加密的字符串
     * @return lee on 2017-08-09 10:51:44
     * @throws UnsupportedEncodingException
     */
    public static String encode3Des(String key, String srcStr) throws UnsupportedEncodingException {
        byte[] keybyte = hex(key);

        return encode(keybyte, srcStr);
    }

    public static String encode3DesKey(String key, String srcStr) throws UnsupportedEncodingException {
        byte[] keybyte = key.getBytes(encoding);

        return encode(keybyte, srcStr);
    }

    public static String encode(byte[] keybyte, String srcStr) throws UnsupportedEncodingException {

        byte[] src = srcStr.getBytes(encoding);
        try {
            //生成密钥
            SecretKey deskey = new SecretKeySpec(keybyte, DesKeySpec);
            //加密
            Cipher c1 = Cipher.getInstance(DesKeySpec);
            c1.init(Cipher.ENCRYPT_MODE, deskey);

            String pwd = Base64.encodeBase64String(c1.doFinal(src));
            return pwd;
        } catch (java.security.NoSuchAlgorithmException e1) {
            // TODO: handle exception
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /**
     * 3DES解密
     *
     * @param key    加密密钥，长度为24字节
     * @param desStr 解密后的字符串
     * @return lee on 2017-08-09 10:52:54
     */
    public static String decode3Des(String key, String desStr) {
        byte[] keybyte = hex(key);
        return decode(keybyte, desStr);
    }

    public static String decode3DesKey(String key, String desStr) {
        byte[] keybyte;
        try {
            keybyte = key.getBytes(encoding);
            return decode(keybyte, desStr);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return desStr;

    }


    public static String decode(byte[] keybyte, String desStr) {
        Base64 base64 = new Base64();

        byte[] src = base64.decode(desStr);

        try {
            //生成密钥
            SecretKey deskey = new SecretKeySpec(keybyte, DesKeySpec);
            //解密
            Cipher c1 = Cipher.getInstance(DesKeySpec);
            c1.init(Cipher.DECRYPT_MODE, deskey);
            String pwd = new String(c1.doFinal(src), encoding);
            return pwd;
        } catch (java.security.NoSuchAlgorithmException e1) {
            // TODO: handle exception
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }
}
