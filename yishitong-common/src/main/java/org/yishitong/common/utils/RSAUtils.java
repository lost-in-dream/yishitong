package org.yishitong.common.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

public class RSAUtils {
    public static final String CHARSET = "UTF-8";
    public static final String RSA_ALGORITHM = "RSA";
    public static final String  SIGN_ALGORITHMS = "SHA1WithRSA";

    public static Map<String, String> createKeys(int keySize){
        //为RSA算法创建一个KeyPairGenerator对象
        KeyPairGenerator kpg;
        try{
            kpg = KeyPairGenerator.getInstance(RSA_ALGORITHM);
        }catch(NoSuchAlgorithmException e){
            throw new IllegalArgumentException("No such algorithm-->[" + RSA_ALGORITHM + "]");
        }

        //初始化KeyPairGenerator对象,密钥长度
        kpg.initialize(keySize);
        //生成密匙对
        KeyPair keyPair = kpg.generateKeyPair();
        //得到公钥
        Key publicKey = keyPair.getPublic();
        String publicKeyStr = Base64.encodeBase64URLSafeString(publicKey.getEncoded());
        //得到私钥
        Key privateKey = keyPair.getPrivate();
        String privateKeyStr = Base64.encodeBase64URLSafeString(privateKey.getEncoded());
        Map<String, String> keyPairMap = new HashMap<String, String>();
        keyPairMap.put("publicKey", publicKeyStr);
        keyPairMap.put("privateKey", privateKeyStr);

        return keyPairMap;
    }

    /**
     * 得到公钥
     * @param publicKey 密钥字符串（经过base64编码）
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //通过X509编码的Key指令获得公钥对象
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey));
        RSAPublicKey key = (RSAPublicKey) keyFactory.generatePublic(x509KeySpec);
        return key;
    }

    /**
     * 得到私钥
     * @param privateKey 密钥字符串（经过base64编码）
     * @throws Exception
     */
    public static RSAPrivateKey getPrivateKey(String privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //通过PKCS#8编码的Key指令获得私钥对象
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
        RSAPrivateKey key = (RSAPrivateKey) keyFactory.generatePrivate(pkcs8KeySpec);
        return key;
    }

    /**
     * 公钥加密
     * @param data
     * @param publicKey
     * @return
     */
    public static String publicEncrypt(String data, RSAPublicKey publicKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return Base64.encodeBase64URLSafeString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET), publicKey.getModulus().bitLength()));
        }catch(Exception e){
            throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
        }
    }

    /**
     * 私钥解密
     * @param data
     * @param privateKey
     * @return
     */

    public static String privateDecrypt(String data, RSAPrivateKey privateKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.decodeBase64(data), privateKey.getModulus().bitLength()), CHARSET);
        }catch(Exception e){
            throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
        }
    }

    /**
     * 私钥加密
     * @param data
     * @param privateKey
     * @return
     */

    public static String privateEncrypt(String data, RSAPrivateKey privateKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            return Base64.encodeBase64URLSafeString(rsaSplitCodec(cipher, Cipher.ENCRYPT_MODE, data.getBytes(CHARSET), privateKey.getModulus().bitLength()));
        }catch(Exception e){
            throw new RuntimeException("加密字符串[" + data + "]时遇到异常", e);
        }
    }

    /**
     * 公钥解密
     * @param data
     * @param publicKey
     * @return
     */

    public static String publicDecrypt(String data, RSAPublicKey publicKey){
        try{
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, Base64.decodeBase64(data), publicKey.getModulus().bitLength()), CHARSET);
        }catch(Exception e){
            throw new RuntimeException("解密字符串[" + data + "]时遇到异常", e);
        }
    }

    private static byte[] rsaSplitCodec(Cipher cipher, int opmode, byte[] datas, int keySize){
        int maxBlock = 0;
        if(opmode == Cipher.DECRYPT_MODE){
            maxBlock = keySize / 8;
        }else{
            maxBlock = keySize / 8 - 11;
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] buff;
        int i = 0;
        try{
            while(datas.length > offSet){
                if(datas.length-offSet > maxBlock){
                    buff = cipher.doFinal(datas, offSet, maxBlock);
                }else{
                    buff = cipher.doFinal(datas, offSet, datas.length-offSet);
                }
                out.write(buff, 0, buff.length);
                i++;
                offSet = i * maxBlock;
            }
        }catch(Exception e){
            throw new RuntimeException("加解密阀值为["+maxBlock+"]的数据时发生异常", e);
        }
        byte[] resultDatas = out.toByteArray();
        IOUtils.closeQuietly(out);
        return resultDatas;
    }
    
    public static String sign(String content, String myPriKey) {

        try {
            
            byte[] decodePubKey = Base64.decodeBase64(myPriKey);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodePubKey);
            KeyFactory factory = KeyFactory.getInstance(RSA_ALGORITHM);
            PrivateKey privateKey = factory.generatePrivate(keySpec);

            Signature signature = Signature.getInstance(SIGN_ALGORITHMS);

            signature.initSign(privateKey);
            signature.update(content.getBytes(CHARSET));

            byte[] signed = signature.sign();

            return Base64.encodeBase64String(signed);

        } catch (Exception e) {

            throw new RuntimeException(e);
        }

    }
    
    public static boolean verify(String content, String aliPubKey,String aliSign) {
        try {
            
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            byte[] decodePubKey = Base64.decodeBase64(aliPubKey);
            
            PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(decodePubKey));

            Signature signature = Signature.getInstance(SIGN_ALGORITHMS);

            signature.initVerify(pubKey);
            signature.update(content.getBytes(CHARSET));
            
            return signature.verify(Base64.decodeBase64(aliSign));

        } catch (Exception e) {
            
            throw new RuntimeException(e);
        }
    }
    
   public static void main(String[] arg) throws NoSuchAlgorithmException, InvalidKeySpecException {
        
        String pubKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwbvLAoMpMbBuVk/ibWl5ZmxYvIPVb5cj6iV+LujSF2XVN5WB6c25sXXq2F4iVBenok9EychHtIxevtW4Jak/egumC5EkDX9A0bCyCkmWSg01V3wT+AmnqLwZaFMTE5Nqu7u8/MfZfrtENaSON9vi+RIuwS5nK7ne09NnkuF+Ts25oZ1Xr0ew+tUjGTvqWIg4vv8lRr7/h6FMDmfgX6JL4OeOfeEK6uSMkbRb3e9pk4RFF3QDPP12iZhaur6CIFSYQ5f4A26opL3d5/HrncNhbkk2yuqRF2WnQbt+NycU/3JzmWhB8+Rh3/6KCn9ICy7mUDPfJsYTjhXPRczzMqq1nQIDAQAB";
        
        String preKey="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDBu8sCgykxsG5WT+JtaXlmbFi8g9VvlyPqJX4u6NIXZdU3lYHpzbmxderYXiJUF6eiT0TJyEe0jF6+1bglqT96C6YLkSQNf0DRsLIKSZZKDTVXfBP4CaeovBloUxMTk2q7u7z8x9l+u0Q1pI432+L5Ei7BLmcrud7T02eS4X5OzbmhnVevR7D61SMZO+pYiDi+/yVGvv+HoUwOZ+Bfokvg54594Qrq5IyRtFvd72mThEUXdAM8/XaJmFq6voIgVJhDl/gDbqikvd3n8eudw2FuSTbK6pEXZadBu343JxT/cnOZaEHz5GHf/ooKf0gLLuZQM98mxhOOFc9FzPMyqrWdAgMBAAECggEBAIEgzIVWKwTllgWNpVxL6xDrspGkRYTldpjAeMSHhlD2HRG5YygVaWnxMKEazULDqYYz06OzDKg4z4XfKahgkOCmFvyqtx8P7IJXhbP5rGSbPSyclSrENK3xAfZZOOPZZFDRJ3xNMJiZ9TLUBb8Rns1o4yT76uRoNS93PxfVmLyolWv4wwieIttliusVSPc7AR+WRsRB08MyxAwkR+EnKP4muWmYNdBjkPvr7NKPHXFAiCrqnoMnTzMNg6b9zT7RLgwtb1K97clh0RWUQ+sf9kGFeaMO0W1h5FIoP1sp3k3s36+hweguwAoG/Bj+HOZQ1XxN63wPadtPEVwxY+jgCYECgYEA7AGvzky2Gcv30n7umSmBwj8AKpYQbL2t4tJlk+lBriivFmO1pj0XRnmhYXSt1x1gGQgAZ4Eq/U79DBWMxY2nvcFKXRDjS5Sa/ifwIWtsF6O8fwpci6VaTRKkHmkumhjzC+htMBZ91OhxTx8lWZ9m5FU4OaTJKqEJ3XbRHsJaIRECgYEA0iVTCQf7PFXzs0RfpgqPp++ejENGIHSbSOQBVmG7AhdZuXICa3w/r9g2XxekhXvugTSaUI5YboCEyXGr8GxhRV+xn8aGQAKxyU7GA+ovzbcGIS4b5uwxk5HuinYFLVfeZRkXMPwSH1jRojShVjVopW+TdYiU5cTNg6J/cffqi80CgYEAwtPppC7jrYwaO6UZp0sCma6UnpNfX8V+RN9Xh7FpzTI8yAjsLh4EluNV/DPsRsczS7a1TrztFkwczdsEaRm1vgpRL17/pKc7BFFIBO6/PwymUWHmYEnSsAmRfqFqHUA3dD8d1DLdEeQJlj2qAcB7mkdXsQ5+GzjyAedSX0gmS2ECgYAGs54ESbbfY7ZT3zhVqTyPlHqv6m1QF6fkGTFZueAm3uvDmDkviFTXnMOPIMfkYeFnSXAqZX+4dmzFp9Rd5HbtqXGWsNQTMoLM8rAp68v+eXm+kSPep2WlWSAMVlsv6P90peoxa7iAO2rkC12hzC48KGTBeMuW0EHlkscRsWBPeQKBgC27jhFQsAWthksBfQE/5oU+i+HodiYyofdWyE1RZ8vldCg0PzQE5jD1rF6TRCEplx/g21Sa/ufkuW+VxU+oaIAvZEvbAqXskORfDNHOo2pjTJbM3jBlkxMQIeA9h1JgRJYXfER3/5rGxxhtsuk/xj96p50648+GhydCtJDpVFXO";
        
        String key="xUHd<>KxzVCbsgVIwTnc1jtpWn";
        
       String rsaStr= publicEncrypt(key,getPublicKey(pubKey));
       System.out.println(rsaStr);
       
       String rsaStrde= privateDecrypt(rsaStr,getPrivateKey(preKey));
       
       System.out.println(rsaStrde);
       //a1NpT_HXaw9-19B5rKX0qUc2x16p_CTk7Amr5xZv_DXNFne-wsk1SmSFVq8vf8vyKrRjCPUXRAZjW6pJGNtO68M8RBbeYS4UyUBBHR2KeMp5RoPl3UUmTw4Ydco_kKZSdNa70MaB4uRqZPcoIf8qrzn_BQwyzF_3CiCFQMm2-MmjnGqwSiTZNx5RVTDqBVrW6FpEV48fevlyggremjQ1vvsfd8JVQQxC0x0rZkOMxFptI9D0qlM4JEzR6YHx0MJ4CSKDF88-jAzxMZhKv2UTl2bHkMVfJBYrIpcwNhEUwmNvyFwCDvA2dnGA826u6ee9n-0Av64eIdQXEQbnxPcewA
    }

}
