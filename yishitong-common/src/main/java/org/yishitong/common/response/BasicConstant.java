package org.yishitong.common.response;

import java.io.Serializable;


/**
 * 基本常量。
 * @author 
 */
public class BasicConstant {
	
	// =============================================================
    //  数组常量
    // =============================================================

    /** 空的byte数组。 */
    public static final byte[] EMPTY_BYTE_ARRAY        = new byte[0];
    public static final Byte[] EMPTY_BYTE_OBJECT_ARRAY = new Byte[0];

    /** 空的short数组。 */
    public static final short[] EMPTY_SHORT_ARRAY        = new short[0];
    public static final Short[] EMPTY_SHORT_OBJECT_ARRAY = new Short[0];

    /** 空的int数组。 */
    public static final int[]     EMPTY_INT_ARRAY            = new int[0];
    public static final Integer[] EMPTY_INTEGER_OBJECT_ARRAY = new Integer[0];

    /** 空的long数组。 */
    public static final long[] EMPTY_LONG_ARRAY        = new long[0];
    public static final Long[] EMPTY_LONG_OBJECT_ARRAY = new Long[0];

    /** 空的float数组。 */
    public static final float[] EMPTY_FLOAT_ARRAY        = new float[0];
    public static final Float[] EMPTY_FLOAT_OBJECT_ARRAY = new Float[0];

    /** 空的double数组。 */
    public static final double[] EMPTY_DOUBLE_ARRAY        = new double[0];
    public static final Double[] EMPTY_DOUBLE_OBJECT_ARRAY = new Double[0];

    /** 空的char数组。 */
    public static final char[]      EMPTY_CHAR_ARRAY             = new char[0];
    public static final Character[] EMPTY_CHARACTER_OBJECT_ARRAY = new Character[0];

    /** 空的boolean数组。 */
    public static final boolean[] EMPTY_BOOLEAN_ARRAY        = new boolean[0];
    public static final Boolean[] EMPTY_BOOLEAN_OBJECT_ARRAY = new Boolean[0];

    /** 空的Object数组。 */
    public static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /** 空的Class数组。 */
    public static final Class<?>[] EMPTY_CLASS_ARRAY = new Class[0];

    /** 空的String数组。 */
    public static final String[] EMPTY_STRING_ARRAY = new String[0];

    // =============================================================
    //  对象常量
    // =============================================================

    // 0-valued primitive wrappers
    public static final Byte      BYTE_ZERO   = new Byte((byte) 0);
    public static final Short     SHORT_ZERO  = new Short((short) 0);
    public static final Integer   INT_ZERO    = new Integer(0);
    public static final Long      LONG_ZERO   = new Long(0L);
    public static final Float     FLOAT_ZERO  = new Float(0);
    public static final Double    DOUBLE_ZERO = new Double(0);
    public static final Character CHAR_NULL   = new Character('\0');
    public static final Boolean   BOOL_FALSE  = Boolean.FALSE;

    /** 代表null值的占位对象。 */
    public static final Object NULL_PLACEHOLDER = new NullPlaceholder();

    private final static class NullPlaceholder implements Serializable {
        private static final long serialVersionUID = 7092611880189329093L;

        @Override
        public String toString() {
            return "null";
        }

        private Object readResolve() {
            return NULL_PLACEHOLDER;
        }
    }

    /** 空字符串。 */
    public static final String EMPTY_STRING = "";

    /**
     * 上传图片是否加密 0：不加密 1：加密
     */
    public static final String ENCRYPT_OK="1";
    public static final String ENCRYPT_NO="0";

    /**
     * 自定义分隔符逗号
     */
    public static final String SEPARATOR=",";

    /**
     * 平台设置key
     */
    public static final String PLA_SET = "PLA_SET";
    /**
     * 代理商商户查询脱敏
     */
    public static final String AGENT_MERQ_DES = "AGENT_MERQ_DES";
    /**
     * 平台开关设置 0 关闭 1 开启
     */
    public static final String SET_ZERO = "0";
    public static final String SET_ONE = "1";

    /**
     * 提现状态 0:失败; 1：成功; 2:提现中; 3:初始状态
     */

    public static final String CASH_FAIL = "0";
    public static final String CASH_SUCC = "1";
    public static final String CASH_ING = "2";
    public static final String CASH_INIT = "3";

}
