package org.yishitong.common.response;

/**
 * App 请求服务器,Jons 对象的基类
 *
 * @param <T> 业务参数
 * @author plt
 */
public class Request<T> {


	/**
	 * 请求的业务参数
	 */
	private T data;
	/**
	 * App 类型 0安卓1 ios 2 小程序  3 h5
	 */
	private Integer appType;
	/**
	 * App 唯一标志
	 */
	private String appSign;

	/**
	 * 平台id
	 */
	private Integer operatorId;

	private Integer appId;

	private String wxInfo;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Integer getAppType() {
		return appType;
	}

	public void setAppType(Integer appType) {
		this.appType = appType;
	}


	public String getAppSign() {
		return appSign;
	}

	public void setAppSign(String appSign) {
		this.appSign = appSign;
	}


	public String getWxInfo() {
		return wxInfo;
	}

	public void setWxInfo(String wxInfo) {
		this.wxInfo = wxInfo;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}

	@Override
	public String toString() {
		return "Request{" +
				"data=" + data +
				", appType=" + appType +
				", appSign='" + appSign + '\'' +
				", operatorId=" + operatorId +
				", appId=" + appId +
				", wxInfo='" + wxInfo + '\'' +
				'}';
	}
}
