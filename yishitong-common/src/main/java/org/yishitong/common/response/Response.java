package org.yishitong.common.response;

import com.alibaba.fastjson.JSONObject;
import org.yishitong.common.response.ResponseCode;

import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

/**
 * @author arch
 */
public class Response implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 成功
	 */
	public static Response ok() {
		Response reqJson = new Response();
		reqJson.code = "0000";
		reqJson.msg = "成功";
		return reqJson;
	}

	/**
	 * 成功
	 */
	public static Response ok(Object info) {
		Response reqJson = ok();
		reqJson.data = info;
		return reqJson;
	}

	/**
	 * 成功
	 */
	public static Response okContent(Object info) {
		Response reqJson = ok();
		JSONObject json = new JSONObject();
		json.put("content", info);
		reqJson.data = json;
		return reqJson;
	}

	/**
	 * 成功
	 */
	public static Response okList(List<?> list) {
		Response reqJson = ok();
		reqJson.data = new ContentObject(list);
		return reqJson;
	}

	/**
	 * 失败
	 */
	public static Response error(ResponseCode responseCode) {
		Response reqJson = new Response();
		reqJson.code = responseCode.getCode();
		reqJson.msg = responseCode.getMsg();

		return reqJson;
	}

	/**
	 * 失败
	 */
	public static Response errorMeg(String errMsg) {
		Response reqJson = new Response();
		reqJson.code = "-1";
		reqJson.msg = errMsg;
		return reqJson;
	}


	/**
	 * 0:成功
	 */
	private String code;
	/**
	 * 返回内容
	 */
	private Object data = new Object();
	private String msg;
	private String sign;
	private String error;
	private Boolean retData;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Boolean getRetData() {
		return retData;
	}

	public void setRetData(Boolean retData) {
		this.retData = retData;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

    @Override
    public String toString() {
        return "Response{" +
                "code='" + code + '\'' +
                ", data=" + data +
                ", msg='" + msg + '\'' +
                ", sign='" + sign + '\'' +
                ", error='" + error + '\'' +
                ", retData=" + retData +
                '}';
    }
}

class ContentObject {
	private List<?> content;

	ContentObject(List<?> content) {
		this.content = content;
	}

	public List<?> getContent() {
		return content;
	}

	public void setContent(List<?> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", ContentObject.class.getSimpleName() + "[", "]")
				.add("content=" + content)
				.toString();
	}
}