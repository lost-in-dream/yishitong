package org.yishitong.common.response;

public interface ResponseCode {

	
	 /**
     * 获取返回消息码
     */
    String getCode();

    /**
     * 获取消息内容
     */
    String getMsg();
}
