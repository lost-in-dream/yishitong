package org.yishitong.common.response;

import org.yishitong.common.response.ResponseCode;

/**
 * @author Yuefei.Wang
 * @time 2019/5/13 15:16
 * @desc
 */
public enum CommonErrorEnum implements ResponseCode {

    IMPORT_AGENT_NULL_CARD("1001", "卡信息不能为空"),
    IMPORT_AGENT_REPEAT("1002", "请勿重复导入"),
    IMPORT_AGENT_UNREALNAME("1003", "未实名"),
    IMPORT_AGENT_PHONE_WRONGFUL("1004", "手机号不合法"),
    IMPORT_AGENT_OPERATOR_NOT_FOUND("1005", "运营商不存在"),
    IMPORT_AGENT_FORCE_BUT_NOT_FOUND("1006", "强制默认上级，但默认上级不存在"),
    IMPORT_AGENT_UP_AND_DEFAULT_NOT_FOUND("1007", "上级和默认上级都不存在"),
    IMPORT_AGENT_ISFORCE_WRONGFUL("1008", "是否强制参数错误，必须为‘Y’或‘N’。"),
    IMPORT_AGENT_FAILED_INSERT("1009", "写入代理商数据失败"),
    IMPORT_AGENT_FAILED_INSERT_CARDINFO("1010", "写入卡信息数据失败"),

    IMPORT_USER_REPEAT("1011", "请勿重复导入"),
    IMPORT_USER_OPERATOR_NOT_FOUND("1012", "运营商不存在"),
    IMPORT_USER_FORCE_BUT_NOT_FOUND("1013", "强制默认上级，但默认上级不存在"),
    IMPORT_USER_UP_AND_DEFAULT_NOT_FOUND("1014", "上级和默认上级都不存在"),
    IMPORT_USER_ISFORCE_WRONGFUL("1015", "是否强制参数错误，必须为‘Y’或‘N’。"),
    IMPORT_USER_FAILED_INSERT("1016", "写入用户数据失败"),
    IMPORT_USER_CANT_ENSURE_DIRECT_AGENT("1017", "无法确定直属代理"),


    OPERATOR_NOT_MATCH("1018", "运营商不匹配"),
    AGENT_NOT_FOUND("1019", "代理商不存在"),
    AGENT_ACCOUNT_STATUS_CANT_WITHDRAW("1020", "代理商账户状态不可提现"),
    AGENT_ACCOUNT_BALANCE_NOT_ENOUGH("1021", "代理商账户余额不足"),
    CARD_STATUS_NOT_OK("1022", "该卡状态非正常"),
    CARD_UNCERTIFIED("1023", "卡未认证且当前卡信息认证不通过"),
    CARD_NOT_FOUND("1024", "结算卡不存在"),
    WITHDRAW_CANT_REFUND("1025", "该提现订单不可退款"),
    WITHDRAW_STATUS_UPDATE_FAILED("1026", "修改提现订单状态失败"),
    ACCOUNT_UPDATE_FAILED("1027", "账户金额更新失败"),
    AGENT_ACCOUNT_STATUS_UNNORMAL("1028", "代理商账户状态异常"),
    WITHDRAW_ORDER_NOT_FOUND("1029", "提现订单不存在"),
    WITHDRAW_AMOUNT_ERROR("1030", "提现金额错误"),

    WITHDRAW_APP_NOT_FOUND("1031", "APP不存在"),
    IMPORT_AGENT_UP_NOT_FOUND("1032", "非强制默认上级，上级不存在"),
    IMPORT_USER_UP_NOT_FOUND("1033", "非强制默认上级，上级不存在"),
    IMPORT_AGENT_REAL_BUT_NO_CARD("1034", "已实名，但卡信息为空"),
    IMPORT_AGENT_REAL_BUT_CARD_PARAM_LACK("1035", "已实名，但卡信息参数缺失"),
    IMPORT_USER_NEED_REAL("1036", "需要实名信息"),
    SMS_CODE_OVERDUE("1037", "短信验证码已过期"),
    SMS_CODE_ERROR("1038", "短信验证码错误"),
    IMPORT_ONOFF_OFF("1039", "导入开关未开启"),
    IMPORT_BRAND_NOT_FOUND("1040", "品牌不存在"),
    IMPORT_AGENT_USER_INSERT_FAILED("1041", "用户信息写入失败"),
    AGENT_NOT_MATCH("1042", "代理商不匹配"),
    WITHDRAW_ORDER_CANT_CANCEL("1043", "该订单不可取消提现"),
    ACCOUNT_LOG_INSERT_FAILED("1044", "账户流水写入异常"),
    WITHDRAW_INSERT_FAILED("1045", "提现订单写入异常"),
    DEFAULT_CARD_NOT_FOUND("1046", "没有默认卡"),
    OPERATOR_NOT_FOUND("1047", "运营商不存在"),
    USER_NOT_FOUND("1048", "用户不存在"),
    OPERATOR_BRAND_NOT_FOUND("1049", "运营商品牌不存在"),
    PARAM_WRONG_END_GT_START("1050", "参数错误：起始SN码不能大于结束SN码"),
    NOT_ALL_TRANSFERABLE("1051", "SN码中有不可划拨的机具"),
    IMPLEMENT_MODEL_NOT_FOUND("1052", "机具型号不存在"),
    BRAND_MODEL_NOT_MATCH("1053", "品牌和型号不匹配"),
    PARAM_WRONG_SNCODELIST_EMPTY("1054", "参数错误：SN码列表为空"),
    UPDATE_ERROR("1055", "更新数据失败"),
    PARAM_WRONG("1056", "参数错误"),
    IMPLEMENT_STATUS_ERROR("1057", "机具状态不正确"),
    IMPLEMENT_NOT_FOUND("1058", "机具不存在"),
    MIN_WITHDRAW_AMOUNT_ERROR("1059", "提现金额小于最低提现金额"),
    IMPLEMENT_MODEL_NOT_MATCH("1060", "机具型号不匹配"),
    PARAM_WRONG_SN_ERROR("1061", "SN码错误，请核对您的SN码。"),

    NO_MERCHANT("1062", "未匹配到商户，请重新选择MCC"),
    PHONE_EXIST("1063", "手机号已注册"),
    IMPLEMENT_NOT_OPEN("1064", "此机具不可开户"),
    STATISTIC_QUERY_ERR("1065", "仅支持查询月统计数据"),

    ALIPAY_ACCOUNT_NOT_BINDING("1066", "支付宝账户未绑定"),
    WITHDRAW_OPERATION_ERROR("1067", "23：30-01：30，系统维护，请在其他时间段进行提现，给您带来得不便，敬请谅解"),
    IMPORT_AGENT_USER_NEED_REAL("1068", "请前往实名或升级版本"),
    ;

    private String code, msg;

    CommonErrorEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}
