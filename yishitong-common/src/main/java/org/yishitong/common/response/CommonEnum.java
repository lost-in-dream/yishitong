package org.yishitong.common.response;

import org.yishitong.common.response.ResponseCode;

/**
 * 公用的错误代码
 * 
 * @author plt
 *
 */
public enum CommonEnum implements ResponseCode {
	LOGIN_TIMEOUT("0001", "登录过期,请重新登录!"),
	LOGIN_NAME_PASS_NOT("0002", "登录名或密码错误!"),
    INVALID_PARAM("0003", "参数错误!"),
    ACCOUNT_LOGGIN_NO("0004", "账号禁止登录!"),
    OPERATOR_LOGGIN_NO("0005","运营商禁止使用!"),
    APP_LOGGIN_NO("0006","App禁止登录!"),
    PLAT_TIME("0007","平台已经过期!"),
    FILE_TYPE("0008","上传的文件格式不符合!"),
    FILE_MAX_LENGTH("0009","上传文件过大!"),
    FILE_FAIL("0010","上传的文件失败!"),
	FALE_AUTH("0011","插入数据库失败!"),
	SMS_CODE_NOT("0012","手机验证错误!"),
    OPER_DATA_NOT("0013","操作数据错误!"),
	SMS_FALE("0014","短信验证码校验错误!"),
	LOHINNAME_EXIST("0015","该手机号已注册!"),
	PHONE_NOT_EXIST("0016","推荐人手机号不存在!"),
	IMG_CODE_NOT("0017","图片验证码错误!"),
	GET_IMG_CODE_NOT("0018","获取图片验证码错误!"),
    SMS_SET_NOT("0019","获取短信配置错误!"),
	CRE_RUN_LOW("0020","余额不足!"),
	LOGIN_NAME_NOT("0021","账号错误!"),
	REAL_NOT("0022","用户未实名!"),
	VER_SIG_ERR("0023","验证签名失败!"),
	NO_ORD_PEN("0024","无此笔订单"),
	TRA_FIL("0025","交易失败!"),
	TRA_AMO_INC("0026","交易金额不一致!"),
	SYS_EXC("0027","系统异常!"),
	ORD_AMT_NOT("0028","金额格式不正确!"),
	PLATFORM_GET_NOT("0032","未获取平台信息!"),
	MERCHANT_GET_NOT("0033","未获取商户信息!"),
	PLAN_CARD_STAT1_ERR("0039","未知状态!"),
	MERINFO_ERR("0046","用户不存在!"),
	ORDERINFO_ERR("0047","订单不存在!"),
	INVALID_SMSCODE_ERR("0051","短信验证码格式不正确!"),
    PAY_HTTP_ERR("0052","扣款失败!"),
	ORDER_ALSUCESS_ERR("0054","订单已成功!"),
	ORDER_ALFAIL_ERR("0055","订单已失败!"),
	MERREAL_ERR("0057","请您先实名!"),
	MERACCT_ERR("0059","用户账户信息不存在!"),
	DATE_NOT_ERR("0063","请选择时间!"),
	AMT_NOT_ERR("0064","请填写金额!"),
	PHONE_NOT_EMPTY("0069","推荐人手机号不能为空!"),
	SMS_TEM_ERR("0070","获取短信模板失败!"),
	MER_CODE_ERR("0071","商户号错误!"),
	MER_NOT("0072","商户不存在!"),
	MER_THE_ERR("0073","所属关系无法向下级所属!"),
	MER_TYPE_NOT("0077","用户类型错误!"),
	OPER_AGO_NOT("0078","上次的执行未结束,请稍后操作!"),
	PLA_TYPE_NOT("0083","平台不存在此等级!"),
	HAS_MERCHANT("0086","该级别下存在用户,请勿删除!"),
	PARAM_ERR("0087","参数有误,请核对信息后重新填写!"),
	REAL_AUTH_ERR("0090","同一个身份证不能重复认证"),
	CHECK_MD5_ERR("0100","验签失败!"),
	update_pwd("0101","两次密码不一致!"),
	update_sure_pwd("0102","旧密码输入错误!"),
	update_pwd_err("0103","修改密码参数错误!"),
	EXTENSION_LEVEL_ERR("0104","该用户不是您的下级用户!"),
	LOGIN_NAME_FORBIT("0105", "账号异常！禁止登录"),
	LOGIN_PLAN("0106", "功能努力维护中"),
	START_PLAN("0109", "订单超时，无法继续执行！"),
	AUTH_IMG_ERR("0112", "图片获取异常,请重新提交！"),
	HAS_BINDING_ERR("0116","您已经绑定其它用户"),
	MOBILE_NOT_FOUND("0123", "手机号不存在！"),
	ORDER_NOT_FOUND("0124", "订单创建失败！"),
	USER_IS_TRUE("0015","用户已存在!"),	
	OPER_DATA_ERR("0144","操作数据错误！"),
	NO_ACTIVITY("0148","找不到对应活动！"),
	APPLY_QUITUPFEEL_FAIL("0153","对不起,不能重复操作！"),
	NO_BINDING_MOBILE("0154","请选绑定手机号！"),
	NO_PRO("0155","没有此商品！"),
	BUSINESS_PROHIBIT("0156","商家被禁止！"),
	SERVICE_NO_NUMBER("0157","服务次数不足！"),
	EXCEED_NUMBER("0158","超过商品限购数！"),
	NOT_FREQUENT_OPERATION("0159","请勿频繁操作！"),
	STOCK_INSUFFICIENT("0158","商品库存不足！"),
	PRO_BUSY("0159","商品太火,挤爆了,请稍后再试！"),
	NOT_SERVER_DATE("0160","未选择服务日期！"),
	INCORRECT_DATA_FORMAT("0161","错误的数据格式！"),
	NOT_RESERVED("0162","商品不可预约！"),
	NOT_REPEAT_DATE("0163","重复的预定日期！"),
	NOT_PAR_USABLE("0164","店铺禁止登录！"),
	ONLY_ONCE("0165","单次只能预约一次！"),
	MATCH_MANY("0166","请联系管理员校验用户信息！"),
	LOGIN_LOCK("0167","密码错误次数已达上限，请使用找回密码功能！"),
	TOKEN_ERROR("0168","token信息错误，请重试！"),
	FILE_FAIL_WITH("0132","分享背景图片宽度必须为750px"),
	FILE_FAIL_HEIGHT("0133","分享背景图片高度必须为1206px"),
	LOGIN_NAME_SMSPASS_NOT("0134", "登录名或短信验证码错误!"),
	LOGIN_SMS_LOCK("0135","短信错误次数已达上限，请使用找回密码功能！"),
	INVALID_HEAD_PARAM("0136", "请求头必填参数校验失败！"),
	SYMPOS_BACK("1111", "闪银MPOS需要重新爬取数据！"),
	ADDRESS_UNDEFINED("1648", "代理商未配置收货地址！"),
    CARD_BIN_NOT_FOUND("1649", "卡bin不存在，请换卡或联系管理员！"),
    LOGIN_IP("1650", "登录ip改变！"),
    LOGIN_PASS_NOT("1651", "请用超管账户登录!"),
	LOGIN_ERROR("1652", "登录异常!"),
	WX_INVALID_PARAM("1653","小程序登录参数校验失败!"),
	WX_GET_OPENID("1654","调用微信api获取用户openId异常"),
	WX_GET_MOBILE("1655","调用微信api获取用户手机号异常"),
	WX_NO_AGENT("1656","根据当前手机号未查询到用户"),
	WX_SERVICEOPENID_LOGIN_ERROR("1661","根据服务号openId未获取到用户信息"),
	NO_SYNCGRADE("1662","未开启同步等级配置"),
	LOGIN_AGENT_PASS_NOT("1663", "登录密码不正确!"),
	LOGIN_ACCOUNT_EXPIRED("1664", "您的账号已过期，请重置密码后再登录!"),
	MODEL_ID_JIJU("125", "掌门通"),
	MODEL_ID_MAPAI("130", "码牌"),
	MODEL_ID_DAPOS("131", "大POS"),
;
	

	private String code, msg;
	CommonEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return this.code;
	}

	@Override
	public String getMsg() {
		// TODO Auto-generated method stub
		return this.msg;
	}

}
