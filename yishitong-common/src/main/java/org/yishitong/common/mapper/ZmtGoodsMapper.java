package org.yishitong.common.mapper;

import org.apache.ibatis.annotations.Param;
import org.yishitong.common.entity.ZmtGoods;

import java.util.List;

/**
 * 掌门通商品信息定制(ZmtGoods)表数据库访问层
 *
 * @author makejava
 * @since 2022-04-29 17:14:58
 */
public interface ZmtGoodsMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ZmtGoods queryById(Integer id);


}

