package org.yishitong.common.dataSource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource{
    /**
     * 默认数据库连接池，默认的必须是主库
     */
    private String defaultSourceName;

    /**
     * 默认只读数据库连接池
     */
    private String slaveSourceName;
    /**
     * 获取与数据源相关的key 此key是Map<String,DataSource> resolvedDataSources 中与数据源绑定的key值
     * 在通过determineTargetDataSource获取目标数据源时使用
     */
    @Override
    protected Object determineCurrentLookupKey()
    {
        return HandleDataSource.getDataSource();
    }
    public String dataSourceName() {
        return HandleDataSource.getDataSource();
    }
    public String getDefaultSourceName() {
        return defaultSourceName;
    }
    public void setDefaultSourceName(String defaultSourceName) {
        this.defaultSourceName = defaultSourceName;
    }
    public String getSlaveSourceName() {
        return slaveSourceName;
    }
    public void setSlaveSourceName(String slaveSourceName) {
        this.slaveSourceName = slaveSourceName;
    }
}
