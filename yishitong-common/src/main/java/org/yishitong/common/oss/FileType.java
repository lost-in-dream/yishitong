package org.yishitong.common.oss;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class FileType {
	private  static Map<String, String> FILE_TYPE_MAP = new HashMap<String, String>();  
	
	public FileType(){
		FILE_TYPE_MAP.put("FFD8FF", "jpg"); //JPEG (jpg)
        FILE_TYPE_MAP.put("89504E47", "png");  //PNG (png)    
        FILE_TYPE_MAP.put("00000018667479706D70","mp4"); 
        FILE_TYPE_MAP.put("000000206674797069736F6D","mp4");
        FILE_TYPE_MAP.put("0000001C667479706D703432","mp4");
        //0000001C667479706D703432
        FILE_TYPE_MAP.put("68746D6C3E", "html");  //HTML (html) 
        FILE_TYPE_MAP.put("504B0304","zip");    
        FILE_TYPE_MAP.put("52617221","rar"); 
	}
	
	 public  String getFileTypeByStream(byte[] b)    
	    {    
	        String filetypeHex = getFileHexString(b).toUpperCase(); 
	       
	        Iterator<Entry<String, String>> entryiterator = FILE_TYPE_MAP.entrySet().iterator();    
	        while (entryiterator.hasNext()) {    
	            Entry<String,String> entry =  entryiterator.next();    
	            String fileTypeHexValue = entry.getKey();    
	            if (filetypeHex.startsWith(fileTypeHexValue)) {    
	                return entry.getValue();    
	            }    
	        } 
	        return null;    
	    }
	 
	 private  String getFileHexString(byte[] b)    
	    {    
	        StringBuilder stringBuilder = new StringBuilder();    
	        if (b == null || b.length <= 0)    
	        {    
	            return null;    
	        }
	        int rleng=b.length>100?100:b.length;
	        for (int i = 0; i < rleng; i++)    
	        {    
	            int v = b[i] & 0xFF;    
	            String hv = Integer.toHexString(v);    
	            if (hv.length() < 2)    
	            {    
	                stringBuilder.append(0);    
	            }    
	            stringBuilder.append(hv);    
	        }    
	        return stringBuilder.toString();    
	    } 
	
}
