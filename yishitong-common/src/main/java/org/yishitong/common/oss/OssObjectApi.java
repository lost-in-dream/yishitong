package org.yishitong.common.oss;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import org.apache.commons.lang3.StringUtils;
import org.yishitong.common.utils.Base64FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

public class OssObjectApi {

	private Logger logger = LoggerFactory.getLogger(org.yishitong.common.oss.OssObjectApi.class);
	/**
	 * 允许文件格式
	 */
	private String allowSuffix;
	/**
	 * 允许文件大小
	 */
	private long allowSize;
	private String bucketName;
	private String accessId;
	private String accessKey;
	private String hostEndPoint;
	private String uploadPath;

	private FileType fileType = new FileType();

	private String genOssPicUrl(String key, InputStream input, long fileLength)
			throws OSSException, ClientException {
		// 使用默认的OSS服务器地址创建OSSClient对象。
		OSSClient client = new OSSClient(hostEndPoint, accessId, accessKey);
		try {
			// 设置 URL 过期时间为 1 小时
			Date expiration = new Date(System.currentTimeMillis() + 24 * 3600 * 1000 * 100000 * 1000000);
			// 生成 URL
			URL url = client.generatePresignedUrl(bucketName, key, expiration);
			ossUploadFile(client, key, input, fileLength);
			String path = url.getProtocol() + "://" + url.getHost() + url.getPath();

			return path;
		} catch (Exception e) {
			logger.error("genOssPicUrl方法异常！", e);
			return "";
		} finally {
			client.shutdown();
		}

	}

	private void ossUploadFile(OSSClient client, String key, InputStream input, long fileLength)
			throws OSSException, ClientException {

		ObjectMetadata objectMeta = new ObjectMetadata();
		objectMeta.setContentLength(fileLength);

		PutObjectResult po = client.putObject(bucketName, key, input, objectMeta);
		po.getETag();
	}

	public synchronized String uploadFile(AutoFile file, String mk) {
		try {
			String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
			int sufindex = allowSuffix.toLowerCase().indexOf(suffix.toLowerCase());
			if (sufindex == -1) {
				return "notype";
			}
			if (file.getSize() > allowSize) {
				return "max";
			}
			String fileNameNew = uploadPath + "/" + mk + "/" + UUID.randomUUID().toString().replaceAll("-", "") + "."
					+ suffix;

			return genOssPicUrl(fileNameNew, file.getInputStream(), file.getSize());
		} catch (Exception e) {
			e.getStackTrace();
			throw e;
		}
	}

	public synchronized void deleteFile(String filePath) {
		// 使用默认的OSS服务器地址创建OSSClient对象。
		OSSClient client = new OSSClient(hostEndPoint, accessId, accessKey);
		try {
			client.deleteObject(bucketName, filePath);
		} catch (Exception e) {
			logger.error("删除文件异常！", e);
		} finally {
			client.shutdown();
		}
	}

	public String getAllowSuffix() {
		return allowSuffix;
	}

	public void setAllowSuffix(String allowSuffix) {
		this.allowSuffix = allowSuffix;
	}

	public long getAllowSize() {
		return allowSize;
	}

	public void setAllowSize(long allowSize) {
		this.allowSize = allowSize;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getHostEndPoint() {
		return hostEndPoint;
	}

	public void setHostEndPoint(String hostEndPoint) {
		this.hostEndPoint = hostEndPoint;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public synchronized String fileUpload(String base64, String mk, String fileName) {
		String imgPath = Base64FileUtils.base64ToFile("/data/img/", base64, fileName + ".jpg");
		if (StringUtils.isEmpty(imgPath)) {
			return "empty";
		}
		File file = new File(imgPath);
		if (file.length() > allowSize) {
			return "max";
		}
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(hostEndPoint, accessId, accessKey);
		String fileNameNew = uploadPath + "/" + mk + "/" + fileName + ".jpg";
		URL url = ossClient.generatePresignedUrl(bucketName, fileNameNew, new Date(System.currentTimeMillis() + 24 * 3600 * 1000 * 100000 * 1000000));
		String path = url.getProtocol() + "://" + url.getHost() + url.getPath();
		// 上传文件。<yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
		ossClient.putObject(bucketName, fileNameNew, file);
		// 关闭OSSClient。
		ossClient.shutdown();
		file.delete();
		return path;
	}
}
