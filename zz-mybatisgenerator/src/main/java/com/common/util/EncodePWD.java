package com.common.util;


public class EncodePWD
{
    public EncodePWD()
    {
    }

    public static String encode(String pwd)
    {
        MD5 md5 = new MD5();
        int n = 0; // 已加密次数
        int m = 1; // 加密码次数
        while (n < m)
        {
            pwd = md5.getMD5ofStr(pwd); // 加密密码
            n++;
        }
        return pwd;
    }
}
