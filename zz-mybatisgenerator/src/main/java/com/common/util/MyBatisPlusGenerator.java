package com.common.util;
 
import java.sql.SQLException;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class MyBatisPlusGenerator {
 
	public static void main(String[] args) throws SQLException {
//		String projectPath = System.getProperty("user.dir");
		String projectPath = "E:\\yishitong\\zz-mybatisgenerator";
		String databaseUrl="jdbc:mysql://rm-2ze2q267g7d29nav1so.mysql.rds.aliyuncs.com/zmt_service?allowMultiQueries=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&zeroDateTimeBehavior=convertToNull";
		String databaseUsername="rds_zmt";
		String databasePassword="y4#@OR^J";
		String author="Jht";
		String parentPackage="org.yishitong.common";
		//要生成的表
		String[] tables = {"sys_menu"};//{"sys_user","sys_role"}
 
				//1. 全局配置
				GlobalConfig config = new GlobalConfig();
				config.setActiveRecord(true) // 是否支持AR模式
					  .setAuthor(author) // 作者
					  //.setOutputDir("D:\\workspace_mp\\mp03\\src\\main\\java") // 生成路径
					  .setOutputDir(projectPath+"/src/main/java") // 生成路径
					  .setFileOverride(true)  // 文件覆盖
					  .setIdType(IdType.AUTO) // 主键策略
					  .setOpen(false)//生成后打开文件夹
					  .setServiceName("I%sService")  // 设置生成的service接口的名字的首字母是否为I
					  					   // IEmployeeService
					  .setMapperName("%sDao")
					  .setEnableCache(false)// XML 二级缓存
		 			  .setBaseResultMap(true)//生成基本的resultMap
		 			  .setBaseColumnList(true);//生成基本的SQL片段 
				//2. 数据源配置
				DataSourceConfig  dsConfig  = new DataSourceConfig();
				dsConfig.setDbType(DbType.MYSQL)  // 设置数据库类型
						.setDriverName("com.mysql.jdbc.Driver")
						.setUrl(databaseUrl)
						.setUsername(databaseUsername)
						.setPassword(databasePassword);
				 
				//3. 策略配置globalConfiguration中
				StrategyConfig stConfig = new StrategyConfig();
				stConfig.setCapitalMode(true) //全局大写命名
						.setDbColumnUnderline(true)  // 指定表名 字段名是否使用下划线
						.setNaming(NamingStrategy.underline_to_camel) // 数据库表映射到实体的命名策略
						//.setTablePrefix("tbl_")
						.setInclude(tables)  // 生成的表
						//.setExclude(new String[]{"test"}) // 排除生成的表
		                // 自定义实体父类
		                // .setSuperEntityClass("com.baomidou.demo.TestEntity")
		                // 自定义实体，公共字段
		                //.setSuperEntityColumns(new String[]{"test_id"})
		                //.setTableFillList(tableFillList)
		                // 自定义 mapper 父类 默认BaseMapper
		                //.setSuperMapperClass("com.baomidou.mybatisplus.mapper.BaseMapper")
		                // 自定义 service 父类 默认IService
		                // .setSuperServiceClass("com.baomidou.demo.TestService")
		                // 自定义 service 实现类父类 默认ServiceImpl
		                // .setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl")
		                // 自定义 controller 父类
		                .setSuperControllerClass("org.yishitong.common.controller.BaseController");
		                // 【实体】是否生成字段常量（默认 false）
		                // public static final String ID = "test_id";
		                // .setEntityColumnConstant(true)
		                // 【实体】是否为构建者模型（默认 false）
		                // public User setName(String name) {this.name = name; return this;}
		                // .setEntityBuilderModel(true)
		                // 【实体】是否为lombok模型（默认 false）<a href="https://projectlombok.org/">document</a>
//		                .setEntityLombokModel(true);
		                // Boolean类型字段是否移除is前缀处理
		                // .setEntityBooleanColumnRemoveIsPrefix(true)
		                // .setRestControllerStyle(true)
		                // .setControllerMappingHyphenStyle(true)
				
				//4. 包名策略配置 
				PackageConfig pkConfig = new PackageConfig();
				pkConfig.setParent(parentPackage)
						.setMapper("dao")//dao
						.setService("service")//servcie
						//.setController("controller")//controller
						.setEntity("entity")
						.setXml("mapper");//mapper.xml
				
				//5. 整合配置
				AutoGenerator  ag = new AutoGenerator();
				ag.setGlobalConfig(config)
				  .setDataSource(dsConfig)
				  .setStrategy(stConfig)
				  .setPackageInfo(pkConfig);
				
				 // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/templates 下面内容修改，
		        // 放置自己项目的 src/main/resources/templates 目录下, 默认名称一下可以不配置，也可以自定义模板名称
		         TemplateConfig tc = new TemplateConfig();
		         tc.setController("/templatesMybatis/controller.java.vm");
		         tc.setService("/templatesMybatis/service.java.vm");
		         tc.setServiceImpl("/templatesMybatis/serviceImpl.java.vm");
		         tc.setEntity("/templatesMybatis/entity.java.vm");
		         tc.setMapper("/templatesMybatis/dao.java.vm");
		         tc.setXml("/templatesMybatis/mapper.xml.vm");
		        // 如上任何一个模块如果设置 空 OR Null 将不生成该模块。
		         ag.setTemplate(tc);

				
				//6. 执行
				ag.execute();
	}
 
}
