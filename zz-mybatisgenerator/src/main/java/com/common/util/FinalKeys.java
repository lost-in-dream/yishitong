package com.common.util;

/**
 * 
 * 全局通用的关键词
 *  session的key，通过此处声明具体的值，其他地方只能引用变量
 * 
 */
public final class FinalKeys
{
    /**
     * 
     * 用户登录后，保存的用户登录信息对应的session key
     * 
     */
    public static final String userSessionKey = "userInfo"; 
    
    /**
     * 
     * 用户登录后，保存的用户权限信息对应的session key
     * 
     */
    public static final String userSessionPrivilegeKey = "userPrivilegeInfo"; 
    
    /**
     * 
     * 登陆首页
     * 
     */
    public static final String urlLogin = "/login.jsp"; 
    /**
     * 
     * 登陆页提交页面
     * 
     */
    public static final String urlLoginAction = "/login.action";
    /**
     * 
     * 用户管理首页
     * 
     */
    public static final String urlIndexPage = "/index.action";
    /**
     * 
     * 用户管理首页
     * 
     */
    public static final String urlIndexAction = "/index.jsp";

    
}

