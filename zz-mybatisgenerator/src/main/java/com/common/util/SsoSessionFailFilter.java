package com.common.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.common.model.SysUser;


public class SsoSessionFailFilter implements Filter {

	/**
	 * 本过滤器的配置信息
	 */
	protected FilterConfig filterConfig = null;

	/**
	 * 初始化过滤器
	 */

	public void init(FilterConfig filterConfig) throws ServletException {

		this.filterConfig = filterConfig;

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		HttpSession httpSession = httpServletRequest.getSession(true);

		boolean isRedirectlogin = false;
		boolean isLogined = false;
		if (httpSession.getAttribute(FinalKeys.userSessionKey) != null
				&& httpSession.getAttribute("userInfo").getClass()
						.equals(SysUser.class)) {
			isLogined = true;
		}
		String refererURI = httpServletRequest.getHeader("referer");

		httpServletResponse.setHeader("P3P", "CP=CAO PSA OUR");
		httpServletResponse.setHeader("X-XSS-protection", "0; mode=block");

		String requesturi = httpServletRequest.getRequestURI().replaceAll("//",
				"/"); // 请求的页面
		String path = httpServletRequest.getContextPath();
		String basePath = httpServletRequest.getScheme() + "://"
				+ httpServletRequest.getServerName() + ":"
				+ httpServletRequest.getServerPort() + path + "/";
		String requestPath = requesturi;
		if (requesturi.startsWith(path)) {
			requestPath = requesturi.substring(path.length());
		}

		// 如果 用户已登陆 或者 正在进行登陆操作
		// 则 无需跳转到登陆页面
		if (isLogined || requestPath.equals(FinalKeys.urlLogin)
				|| requestPath.equals(FinalKeys.urlLoginAction)|| requestPath.equals("/sendMq.action")) {
			isRedirectlogin = false;
		}
		// 其他情况，则都需进入登陆页
		else {
			isRedirectlogin = true;
		}

		if (isRedirectlogin) {
			httpServletResponse.sendRedirect(basePath + "login.jsp");
		} else {
			chain.doFilter(request, response);
		}
	}

	public void destroy() {

	}

	protected void forward(ServletRequest request, ServletResponse response,
			String url) throws IOException, ServletException {
		javax.servlet.RequestDispatcher rd = request.getRequestDispatcher(url);
		rd.forward(request, response);
	}

}
