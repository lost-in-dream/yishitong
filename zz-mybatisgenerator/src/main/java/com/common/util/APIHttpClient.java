package com.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.SSLContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * 支持http和https的真正post传输
 * @author Administrator
 *
 */

public class APIHttpClient {
	// 接口地址
	private String apiURL = "";
	private Log logger = LogFactory.getLog(this.getClass());
	private static final String pattern = "yyyy-MM-dd HH:mm:ss:SSS";
	private HttpClient httpClient = null;
	private HttpPost method = null;
	private long startTime = 0L;
	private long endTime = 0L;
	private int status = 0;

//	public static void main(String[] args) {
//		APIHttpClient a=new APIHttpClient();
//		a.post("http://mdcs.hnyangshumiao.cn:8080/maidong/sendMq.action","{\"sendContent\":{\"url\":\"http://www.mdzjia.com//index.php/Home/api/getNewHerbsinfo\",\"param\":{\"ver\":\"100\",\"name\":\"张辉\"}}}");
//	}
	/**
	 * 调用 API
	 * 
	 * @param parameters
	 * @return
	 */
	public String post(String url, String parameters)

	{
		String body = null;
		// 新建一个HttpPost请求的对象 --并将uri传给这个对象
		
		HttpPost post = new HttpPost(url);
		try {
	
			logger.info("parameters:" + parameters);
			// 新建一个客户对象
	
			HttpClient client = null;
			
			List<NameValuePair> list = new ArrayList<NameValuePair>();
			if(parameters==null){
			}else{
				// 使用NameValuePair 键值对 对参数进行打包
				JSONObject jsonObject = JSONObject.fromObject(parameters);
				logger.info("json:" + jsonObject.toString());
				
				Iterator<String> it = jsonObject.keys(); 
				while(it.hasNext()){
					// 获得key
					String key = it.next(); 
					String value = jsonObject.getString(key);    
					list.add(new BasicNameValuePair(key, value));
				}
			}
			// https
            if (url.substring(0, 5).equals("https")) {
            	httpClient = new SSLClient();  
                if (list.size() > 0) {  
                    UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list,Consts.UTF_8);  
                    post.setEntity(entity);  
                }  
                HttpResponse response = httpClient.execute(post);  
                if (response != null) {  
                    HttpEntity resEntity = response.getEntity();  
                    if (resEntity != null) {  
                    	body = EntityUtils.toString(resEntity, Consts.UTF_8);  
                    }  
                }  
            }else{
            	 client = HttpClients.createDefault();
		
				// 4.对打包好的参数，使用UrlEncodedFormEntity工具类生成实体的类型数据
		
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list,Consts.UTF_8);
		
				// 5.将含有请求参数的实体对象放到post请求中
				post.setEntity(entity);
		
				startTime = System.currentTimeMillis();
				// 6.新建一个响应对象来接收客户端执行post的结果
				HttpResponse response = client.execute(post);
				int statusCode = response.getStatusLine().getStatusCode();
				logger.info("statusCode:" + statusCode);
				logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));
				if (statusCode != HttpStatus.SC_OK) {
					logger.error("Method failed:" + response.getStatusLine());
					status = 1;
				}
		
				endTime = System.currentTimeMillis();
				// 7.从响应对象中提取需要的结果-->实际结果,与预期结果进行对比
				if (response.getStatusLine().getStatusCode() == 200) {
		
					body=EntityUtils.toString(response.getEntity());
		
				}
            }
		} catch (Exception e) {
			// 发生网络异常
			logger.error("exception occurred!\n"+ e.getMessage());
			// 网络错误
			status = 3;
		} finally {
			//关掉http的链接
			post.abort();
			logger.info("调用接口状态：" + status);
		}

		return body;
	}
	public static void main(String[] args) {
		APIHttpClient http=new APIHttpClient();
		System.out.println(http.post("https://www.baidu.com", null));
	}
	

	/**
	 * 0.成功 1.执行方法失败 2.协议错误 3.网络错误
	 * 
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public long getEndTime() {
		return endTime;
	}
}