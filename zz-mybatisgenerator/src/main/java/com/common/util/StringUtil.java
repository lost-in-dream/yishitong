package com.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;



public class StringUtil
{
    
    public static String getSystemPropertie(String key){
    	Properties prop = new Properties();
    	InputStream isInputStream = null;
	  try {
          isInputStream = StringUtil.class.getResourceAsStream("/system.properties");
          prop.load(isInputStream);
          return prop.getProperty(key);
         
      } catch (IOException e) {
          e.printStackTrace();
      }finally {
          try {
              if(isInputStream != null) {
                  isInputStream.close();
              }
          } catch (IOException e) {
              isInputStream = null;
          }
      }
      return null;
    }
	
    // public string() {
    // }

    /*
     * 字符串截取函数 此函数是一个字符串的截取函数解决了中文由于双字节截取会出现乱码的问题。 思路如下：首先判断截取的长度是否是偶数（因为中文是双字节的）如果不是maxLength+1，
     * 然后循环查找被截取的那段字符串中单字节字符的个数是否偶数个如果不是len+1， 最后得出的就是被截取的长度len。
     * @param getStirng 截取字符串
     * @parqm maxLength 要截取的长度
     * @return String 截取后的字符串
     */
    public static String substring(String getString, int maxLength)
    {
        String outString = "";
        int len = 0;
        try 
        {
            byte[] target_byte = getString.getBytes("ISO8859_1");
            getString = new String(target_byte, "ISO8859_1");
            byte[] byteStr = getString.getBytes("ISO8859_1");
            if (maxLength >= 1)
            {
                if ((maxLength % 2) != 0)
                {
                    maxLength += 1;
                }
                if (getString.length() <= maxLength)
                {
                    outString = getString;
                }
                else
                {
                    int enNumber = 0;
                    for (int i = 0; i < maxLength; i++)
                    {
                        len++;
                        Byte byteTemp1 = new Byte(byteStr[i]);
                        if (byteTemp1.intValue() >= 0)
                        {
                            enNumber++;
                        }
                    }
                    if ((enNumber % 2) != 0)
                    {
                        len += 1;
                    }
                    outString = getString.substring(0, len);
                }
            }
            else
            {
                outString = getString;
            }
        }
        catch (Exception e)
        {
        }
        return outString;
    }

    /*
     * 字符串替换函数 将strSource中的所有strFro替换为strTo
     * @param strSource 源字符串
     * @param strFrom 要替环字符串
     * @param strTo 替换后的字符串
     * @return String 将strSource中的所有strFro替换为strTo后的字符串
     */
    public static String replace(String strSource, String strFrom, String strTo)
    {
        java.lang.String strDest = "";
        int intFromLen = strFrom.length();
        int intPos;

        while ((intPos = strSource.indexOf(strFrom)) != -1)
        {
            strDest = strDest + strSource.substring(0, intPos);
            strDest = strDest + strTo;
            strSource = strSource.substring(intPos + intFromLen);
        }
        strDest = strDest + strSource;

        return strDest;
    }

    /*
     * 字符串分割函数将字符串 s 分割成以delimiter为分界的字符数组
     * @param s 代分割字符串
     * @param delimiter 分界字符串
     * @return String[]
     */
    public static String[] split(String s, String delimiter)
    {
        if (s == null || delimiter == null)
        {
            return new String[0];
        }

        if (!s.endsWith(delimiter))
        {
            s += delimiter;
        }
        s = s.trim();

        if (s.equals(delimiter))
        {
            return new String[0];
        }

        List nodeValues = new ArrayList();
        if (delimiter.equals("\n") || delimiter.equals("\r"))
        {
            try
            {
                BufferedReader br = new BufferedReader(new StringReader(s));
                String line = null;
                while ((line = br.readLine()) != null)
                {
                    nodeValues.add(line);
                }
                br.close();
            }
            catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
        }
        else
        {
            int offset = 0;
            int pos = s.indexOf(delimiter, offset);
            while (pos != -1)
            {
                nodeValues.add(s.substring(offset, pos));
                offset = pos + delimiter.length();
                pos = s.indexOf(delimiter, offset);
            }
        }
        return (String[]) nodeValues.toArray(new String[0]);
    }

    /**
     * 替换大文本输入框中的回车、换行、空格
     * 
     * @param strSource
     *            String
     * @return String
     */
    public static java.lang.String replaceBr(java.lang.String strSource)
    {
        java.lang.String strDest = "";
        strDest = replace(strSource, "&", "&amp;");
        strDest = replace(strDest, "<", "&lt;");
        strDest = replace(strDest, ">", "&gt;");
        strDest = replace(strDest, "\t", "    ");
        strDest = replace(strDest, "\r\n", "\n");
        strDest = replace(strDest, "\n", "<br>");
        strDest = replace(strDest, "  ", " &nbsp;");
        return strDest;
    }

    public static String getTimeFormate(String second)
    {
        String rs = "";
        if (second != null && second.trim().length() > 0)
        {
            second = second.trim();
            int seconds = Integer.parseInt(second);
            int s_h = 0;
            int s_e = 0;
            String hour = "00";
            String minu = "00";
            String seco = "00";
            String splitstr = ":";
            int tmp = 0;
            if (seconds >= 3600)
            {
                s_h = seconds / 3600;
                s_e = seconds % 3600;
                rs = s_h + "时";
            }
            else
            {
                s_e = seconds;
            }
            if (s_e >= 60)
            {
                s_h = s_e / 60;
                s_e = s_e % 60;
                rs = rs + s_h + "分";
            }
            else if (s_h > 0)
            {
                rs = rs + "0分";
            }
            rs = rs + s_e + "秒";

        }
        return rs;
    }

    /**
     * input输入框中输入“‘”时会发生错误，本方法将这其替换成“‘’”
     * 
     * @param s
     *            String
     * @return String
     */
    public static String toDBSafeString(String s)
    {
        String result = "";
        char c;
        if (s == null)
        {
            return "";
        }
        else
        {
            for (int i = 0; i < s.length(); i++)
            {
                c = s.charAt(i);
                if ((int) c == 39)
                {
                    result += "''";
                }
                else if ((int) c == 92)
                {
                    result += "\\\\";
                }
                else
                {
                    result += c;
                }
            }
        }
        return result;
    }

//    public static void main(String[] args)
//    {
//        /*
//         * String aa = "我AC中国心123你好！"; int max = aa.getBytes().length;
//         * System.out.println("Stirng aa=" + aa + ", max bytes len=" + max); for (int i = 0; i < max
//         * + 2; i++) { System.out.println("subString(aa," + i + ")=" + substring(aa, i)); }
//         * System.out.println(StringUtil.toDBSafeString("\\sddfsd'sdfs\\d/f")); String test="0";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="1";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="59";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="60";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="61";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="119";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="120";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="3599";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="3600";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="3601";
//         * System.out.println(test+"秒"+getTimeFormate(test)); test="68754";
//         * System.out.println(test+"秒"+getTimeFormate(test));
//         */
//        // System.out.println(StringUtil.fa_area("1.2.7.3"));
//        // System.out.println(StringUtil.fa_menuId("001013006007002001002001"));
//    }

    /**
     * 根据区域ID，通过"."，截取字符串获取父级区域ID
     * 
     * @param areaId
     *            1.2.7.3
     * @return '','1.2.7.3','1.2.7','1.2','1'
     */
    public static String fa_area(String areaId)
    {
        String[] area = areaId.split("\\.");
        String str = "'','" + areaId + "'";
        for (int i = 0; area != null && i < area.length; i++)
        {
            int index = areaId.lastIndexOf(".");
            if (index >= 0)
            {
                areaId = areaId.substring(0, index);
                str += ",'" + areaId + "'";
            }
        }
        return str;
    }

    /**
     * 根据知识体系ID，通过ID生成规则(两位一级)，通过截取字符串的方法，算出该知识体系的父级ID
     * 
     * @param ksId
     *            000902120503
     * @return '00','0009','000902','00090212','0009021205','000902120503'
     */
    public static String fa_ksId(String ksId)
    {
        String str = "''";
        int f_count = ksId.length() / 2;
        for (int i = 0; i < f_count; i++)
        {
            str += ",'" + ksId.substring(0, i * 2 + 2) + "'";
        }
        return str;
    }

    /**
     * 根据栏目编码MENU_CODE,根据CODE生成规则(三位一级)，通过截取字符串的方法，算出该栏目的父级CODE
     * 
     * @param menuId
     *            001013006007002001002001
     * @return '','001','001013','001013006','001013006007','001013006007002','001013006007002001','001013006007002001002','001013006007002001
     *         0 0 2 0 0 1 '
     */
    public static String fa_menuId(String menuCode)
    {
        String str = "''";
        int f_count = menuCode.length() / 3;
        for (int i = 0; i < f_count; i++)
        {
            str += ",'" + menuCode.substring(0, i * 3 + 3) + "'";
        }
        return str;
    }

    /**
     * [判断字符串是否为null为""]
     * 
     * @param str
     * @return boolean true:为null或""
     */

    public static boolean isEmpty(String str)
    {
        return null == str || "".equals(str.trim());
    }

    /**
     * [判断字符串是否为long]
     * 
     * @param str
     * @return boolean
     */

    public static boolean isLong(String str)
    {
        try
        {
            if (isEmpty(str))
            {
                return false;
            }

            Long.valueOf(str.trim());
        }
        catch (NumberFormatException ne)
        {
            return false;
        }

        return true;
    }
  

    /**
     * [判断字符串是否为数字]
     * 
     * @param str
     * @return boolean
     */

    public static boolean isInteger(String str)
    {
        try
        {
            if (isEmpty(str))
            {
                return false;
            }

            Integer.valueOf(str);
        }
        catch (NumberFormatException ne)
        {
            return false;
        }

        return true;
    }

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static Date getDateFromString(String str, String format)
    {
        Date ret = null;
        try
        {
            if ((str == null) || (str.equals("")))
            {
                return null;
            }
            str = str.trim();
            SimpleDateFormat fomater = new SimpleDateFormat(format);
            ret = fomater.parse(str);
        }
        catch (Exception e)
        {
            ret = null;
        }
        return ret;
    }

    public static String getStringFromDate(Date date, String formate)
    {
        if (date == null)
        {
            return "";
        }
        String formatDate = null;
        try
        {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formate);
            formatDate = simpleDateFormat.format(date);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (formatDate == null) formatDate = "";
        return formatDate;
    }

    public static Date getDateTimeFromString(String strDate)
    {
        return getDateFromString(strDate, DATE_TIME_FORMAT);
    }

    public static String getStringFromDate(Date date)
    {
        return getStringFromDate(date, DATE_FORMAT);
    }

    /**
     * unicode 转换成中文
     * @param theString
     * @return
     */
    public static String decodeUnicode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        
        for (int x = 0; x < len;) {

            aChar = theString.charAt(x++);

            if (aChar == '\\') {

                aChar = theString.charAt(x++);

                if (aChar == 'u') {

                    // Read the xxxx

                    int value = 0;

                    for (int i = 0; i < 4; i++) {

                        aChar = theString.charAt(x++);

                        switch (aChar) {

                        case '0':

                        case '1':

                        case '2':

                        case '3':

                        case '4':

                        case '5':

                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            value = (value << 4) + aChar - '0';
                            break;
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':
                            value = (value << 4) + 10 + aChar - 'a';
                            break;
                        case 'A':
                        case 'B':
                        case 'C':
                        case 'D':
                        case 'E':
                        case 'F':
                            value = (value << 4) + 10 + aChar - 'A';
                            break;
                        default:
                            throw new IllegalArgumentException(
                                    "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';

                    else if (aChar == 'n')

                        aChar = '\n';

                    else if (aChar == 'f')

                        aChar = '\f';

                    outBuffer.append(aChar);

                }

            } else {
                outBuffer.append(aChar);
            }

        }
        
        return outBuffer.toString();
    }
    
    /**
     * 将中文字符转行为UTF8编码
     * @param str
     * @return
     */
    public static String convert(String str) 
    { 
        StringBuffer sb = new StringBuffer(1024); 
        sb.setLength(0); 
        for(int i=0; i<str.length(); i++) 
        { 
            char c = str.charAt(i); 
            if (c > 255) 
            { 
                sb.append("\\u"); 
                int j = (c >>> 8); 
                String tmp = Integer.toHexString(j); 
                if (tmp.length() == 1) 
                { 
                    sb.append("0"); 
                } 
                sb.append(tmp); 
                j = (c & 0xFF); 
                tmp = Integer.toHexString(j); 
                if (tmp.length() == 1) 
                { 
                    sb.append("0"); 
                } 
                sb.append(tmp); 
            } 
            else 
            { 
            sb.append(c); 
            } 
        } 
        return(new String(sb)); 
    } 
    
}
