
package com.common.util;

import lombok.Data;

/*
 * 
 * 异步提交时返回的json数据格式
 * 
 */
@Data
public class SubmitMessage implements java.io.Serializable
{
    private boolean success = true;

    private String info;
    
    private String data;
}
