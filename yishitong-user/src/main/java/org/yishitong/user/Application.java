package org.yishitong.user;

import org.springframework.boot.SpringApplication;

import feign.Retryer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;

@ImportResource("applicationContext.xml")
@SpringBootApplication(scanBasePackages = "org.yishitong")
@EnableDiscoveryClient
@EnableFeignClients
@EnableHystrix
@EnableAsync
//@PropertySource(value={"file:/home/config/dbconfig.properties"},ignoreResourceNotFound = true)
@PropertySource(value={"classpath:application.properties"},ignoreResourceNotFound = true)
public class Application {
    public static void main( String[] args )
    {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    Retryer feignRetryer() {
        return Retryer.NEVER_RETRY;
    }
}
